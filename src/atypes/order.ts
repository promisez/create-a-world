// 设备位置信息列表
export interface LocationList {
  address: string
  device_id: string
  latitude: string
  longitude: string
  name: string
  state: string
  distance: number
  friend_group: string
  online: string
}


// 杯子列表
export interface CupList {
  cap: string;
  color: string;
  model_id: string;
  name: string;
  pic: string;
  price: string;
  desc: string;
}

export interface UploadProp {
  filePath: string;
  onSuccess: (res: any) => void;
  onFail: (err: any) => void;
  formData?: any;
}

export interface LogoList {
  id: number;
  image_url: string;
  likes_count: number;
  name: string;

}