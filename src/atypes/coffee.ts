export interface CoffeeTypeList {
  class_id: number;
  class_name: string;
  coffees_id: string[];
  coffees: coffeeItem[];
}

export interface coffeeItem {
  model_id: string;
  hot_model_id: string;
  name: string;
  price: string;
  pic: string;
  base_ice: string; // 默认的温度
  base_sweet: string[]; // ["10", "10", "", "", ""];
  option: {
    ice: string[]; // ["热", "免冰", "三分冰", "五分冰", "七分冰", "全冰"];
    sweet: string[]; //["免糖", "三分糖", "五分糖", "七分糖", "全糖"];
  };
}

// 加入购物车的咖啡
export interface currentCarCoffeeInfo extends coffeeItem {
  currentNum: number;
  ice: string;
  sweet: string;
  dataId: number;
  checked: boolean;
}

// 加入购物的物品
export interface shopCarInfo {
  coffee: currentCarCoffeeInfo[];
  cup: any[];
  image: any[];
  isChecked: boolean;
}
