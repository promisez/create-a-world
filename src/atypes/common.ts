export interface commonRequestRes<T> {
  cookies: [];
  data: T;
  errMsg: string;
  header: any;
  statusCode: 200;
}

export type ResProp<T> = {
  Code: number;
  Data: T;
}