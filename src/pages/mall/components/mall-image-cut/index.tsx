import { View, Button, Image, Text } from "@tarojs/components";
import Taro from "@tarojs/taro";
import { useState } from "react";
import TaroCropper from "taro-cropper";
// import { ImageClipper } from "taro-image-clipper";
// import { ImageClipperPrivate } from "../image-clipper";
import icons from "@/common/icon";
import "./index.less";
import { uploadImg } from "@/api/order";
import { toast } from "@/api/util";
import { ResProp } from "@/atypes";

export default function MallImageCut({ onCut, ccurrentImg = '', isShowCut = false, onCancel }) {
  // 是否展示裁剪器
  // const [showClipper, setShowClipper] = useState(false);
  // 选择的原始图片
  // const [originalImage, setOriginalImage] = useState("");
  // 裁剪后图片
  // const [clippedImage, setClippedImage] = useState("");
  // const onclick = () => {
  //   Taro.chooseImage({
  //     count: 1,
  //   }).then((res) => {
  //     setShowClipper(true);
  //     setOriginalImage(res.tempFilePaths[0]);
  //   });
  // };
  const onCutImg = (res) => {
    Taro.showLoading({ title: "裁剪中" });
    uploadImg({
      filePath: res,
      onSuccess: function (res) {
        const data = JSON.parse(res.data);
        if (data?.Code == 200) {
          Taro.hideLoading();
          toast("裁剪成功");
          onCut(data.URL);
          onCancel()
          return;
        }
        toast("裁剪失败");
      },
      onFail: function (err) {
        Taro.hideLoading();
        toast("裁剪失败");
      },
    });
  };
  return (
    <View className="mallImageCutContainer">
      {isShowCut && (
        <View className="cropper-container">
          <TaroCropper
            cropperWidth={600}
            cropperHeight={600}
            maxScale={8}
            hideCancelText={false}
            quality={10}
            onCancel={() => {
              console.log("点击了取消");
              onCancel()
            }}
            onFail={(err) => {
              console.log(err, "裁剪失败");
              onCancel()
            }}
            src={ccurrentImg}
            fullScreen
            onCut={onCutImg}
          />
        </View>
      )}
    </View>
  );
}
