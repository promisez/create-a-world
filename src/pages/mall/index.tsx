import { Input, ScrollView, View, Image } from "@tarojs/components";
import React, { useEffect, useState } from "react";
import { Tabs } from "@taroify/core";
import ImageDetail from "@/components/image-detail";
import Headers from "@/components/common-header";
import NavTopBar from "@/components/nav-bar";
import { tabData, collectList } from "./index.preset";
import "./index.less";
import Taro from "@tarojs/taro";
import OrderStepHeader from "../order/components/order-header";
import icons from "@/common/icon";

function Mall() {
  const [tabList, setTabList] = useState(tabData);
  const [datalist, setDatalist] = useState(collectList);

  const onTabClick = (e) => {
    e.stopPropagation();
  };
  return (
    <View className="mallContent">
      <NavTopBar />
      <Headers title="共享广场" isShowBack={false} />
      <Tabs onClick={onTabClick} sticky animated className="mallTab">
        {tabList.map((item, index) => {
          return (
            <Tabs.TabPane key={index} title={item.title}>
              <ScrollView className="mall-item-container" scrollY scrollWithAnimation scrollTop={0}>
                <View className="item-content">
                  {datalist.map((v, i) => (
                    <View key={v.url + i} className="mallImageItem">
                      <ImageDetail index={i} data={v} />
                    </View>
                  ))}
                </View>
                <View className="auto-height" />
              </ScrollView>
            </Tabs.TabPane>
          );
        })}
      </Tabs>

      <View className="bottomButton">
        <View
          className="no-choose"
          onClick={() => Taro.navigateTo({ url: "/pages/MallScreens/publish/index" })}
        >
          <Image src={icons.icon_airplane} className="shop-car-img" />
          <View>发布</View>
        </View>
      </View>
    </View>
  );
}

export default Mall;
