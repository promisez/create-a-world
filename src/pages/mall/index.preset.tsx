// import AllImage from './components/allImage'

export const tabData = [
  {
    id: 0,
    title: "全部",
  },
  {
    id: 1,
    title: "抖音爆款",
  },
  {
    id: 2,
    title: "卡通",
  },
  {
    id: 3,
    title: "美食",
  },
  {
    id: 4,
    title: "治愈",
  },
  {
    id: 5,
    title: "萌宠",
  },
  {
    id: 6,
    title: "没了",
  },
];
export const collectList: any[] = [];

Array(6)
  .fill(0)
  .forEach((item, index) => {
    collectList.push({
      url: "https://file.psd.cn/2022/02-18/8d7eccb968c7bbc1d600d7e41e56f7d7.jpg",
      title: "我是收藏款标题",
    });
  });
