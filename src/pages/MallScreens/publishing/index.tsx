import NavTopBar from "@/components/nav-bar";
import { Image, View } from "@tarojs/components";
import React from "react";
import Headers from "@/components/common-header";
import Taro from "@tarojs/taro";
import icons from "@/common/icon";

import "./index.less";

function Publishing() {
  return (
    <View className="publishing_container">
      <NavTopBar />
      <Headers title="发布" />
      <View className="publish_content">
        <View className="content_box">
          <Image src={icons.icon_fabu} className="publishing_icon" />
          <View className="publish_status">发布中，等待审核</View>
          <View className="publish_tip">审核结果可在[我]-[我的发布]列表中查</View>
        </View>
        <View className="back_mall" onClick={() => Taro.switchTab({ url: "/pages/mall/index" })}>
          返回广场
        </View>
      </View>
    </View>
  );
}

export default Publishing;
