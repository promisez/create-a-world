import NavTopBar from "@/components/nav-bar";
import { PageContainer, View, Image, ScrollView } from "@tarojs/components";
import Headers from "@/components/common-header";
import React, { useEffect, useState } from "react";
import icons from "@/common/icon";
import { Textarea, Input, Field } from "@taroify/core";
import PopUpHeader from "@/components/popup-header";
import Taro from "@tarojs/taro";
import { getTagList } from "@/api/mall";
import "./index.less";
import { tagArr } from "./index.preset";
import MallImageCut from "../../mall/components/mall-image-cut";

function Publish() {
  const [showSelectTag, setShowSelectTag] = useState(false);
  const [title, setTitle] = useState("");
  const [desc, setDesc] = useState("");
  const [tags, setTags] = useState("");
  const [imgUrl, setImgUrl] = useState("");

  function isToNext() {
    return title.length && desc.length && tags.length && imgUrl.length;
  }

  // 获取标签列表
  function getTagsFn() {
    getTagList()
      .then((res) => {
        console.log(res, "标签列表");
      })
      .catch((err) => {
        console.log(err, "获取标签失败");
      });
  }

  useEffect(() => {
    getTagsFn();
  }, []);

  // 选择的原始图片
  const [originalImage, setOriginalImage] = useState("");
  const [showClipper, setShowClipper] = useState(false);
  const chooseImageFn = () => {
    Taro.chooseImage({
      count: 1,
    }).then((res) => {
      setShowClipper(true);
      setOriginalImage(res.tempFilePaths[0]);
    });
  };

  return (
    <View className="publish_container">
      <NavTopBar />
      <Headers title="发布" />
      <View className="publish_content">
        <View className="content_box">
          <View className="upload_box" onClick={chooseImageFn}>
            {imgUrl.length ? (
              <View className="current_img_box">
                <Image src={imgUrl} className="current_img" />
                <View className="tip">点击可重新选择</View>
              </View>
            ) : (
              <View className="upload_icon_box">
                <Image src={icons.icon_upload2} className="upload_icon" />
                <View className="upload_text">点击上传图片</View>
              </View>
            )}
          </View>
          <View>
            <MallImageCut
              onCut={(val) => setImgUrl(val)}
              ccurrentImg={originalImage}
              isShowCut={showClipper}
              onCancel={() => setShowClipper(false)}
            />
          </View>
          <View className="input_box">
            <Input
              placeholder="请输入标题"
              value={title}
              onChange={(e: any) => setTitle(e.target.value)}
              className="input_title"
              maxlength={24}
            />
            <View className="num">{`${title.length}/24`}</View>
            <Textarea
              limit={100}
              placeholder="请输入描述"
              value={desc}
              className="input_desc"
              onChange={(e: any) => setDesc(e.target.value)}
            />
            <View className="choose-tag" onClick={() => setShowSelectTag(true)}>
              {!!tags?.length ? (
                <View className="active_tag">{tags}</View>
              ) : (
                <View className="text">请选择标签</View>
              )}
              <Image src={icons.icon_btn_down1} className="icon" />
            </View>
          </View>
        </View>
      </View>

      <View
        className="bottomButton"
        onClick={() => {
          if (isToNext()) {
            Taro.navigateTo({ url: "/pages/MallScreens/publishing/index" });
          }
        }}
      >
        <View className={isToNext() ? "text-box activeButton" : "text-box"}>
          <View className="text">提交</View>
        </View>
      </View>

      <PageContainer
        show={showSelectTag}
        className="tag_container"
        position="bottom"
        round
        onAfterLeave={() => setShowSelectTag(false)}
      >
        <View className="header_btn">
          <PopUpHeader onClick={() => setShowSelectTag(false)} />
        </View>
        <ScrollView
          className="scroll_container"
          scrollY
          scrollWithAnimation
          scrollTop={0}
          style={{ height: "304px" }}
        >
          {tagArr.map((it, index) => {
            return (
              <View
                className="tag_item"
                key={index}
                onClick={() => {
                  setTags(it.name);
                  setShowSelectTag(false);
                }}
              >
                <View className="tag_text">{it.name}</View>
              </View>
            );
          })}
        </ScrollView>
        <View className="cancel_btn" onClick={() => setShowSelectTag(false)}>
          取消
        </View>
      </PageContainer>
    </View>
  );
}

export default Publish;
