export const tagArr = [
  {
    id: 1,
    name: "抖音爆款",
    isSelect: true,
  },
  {
    id: 2,
    name: "卡通",
    isSelect: false,
  },
  {
    id: 3,
    name: "美食",
    isSelect: false,
  },
  {
    id: 4,
    name: "治愈",
  },
  {
    id: 5,
    name: "萌宠",
  },
  {
    id: 6,
    name: "美女",
  },
  {
    id: 7,
    name: "夏日",
  },
];
