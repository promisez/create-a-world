// 防抖
export function debounce(fn, delay) {
  return (args) => {
    clearTimeout(fn.id);

    fn.id = setTimeout(() => {
      fn.call(this, args);
    }, delay);
  };
}
