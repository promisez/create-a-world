import icons from "@/common/icon";
import NavTopBar from "@/components/nav-bar";
import { Image, PageContainer, ScrollView, View } from "@tarojs/components";
import Taro, { useDidShow, useShareAppMessage } from "@tarojs/taro";
import { useEffect, useState } from "react";
import { calculatePrice, calculatePriceRes, getCoffeeTypeList, getCouponCard } from "@/api/coffee";
import { CoffeeTypeList, LocationList, ResProp } from "@/atypes";
import PopUpHeader from "@/components/popup-header";
import { getLoad, setLoad, shareIntercept } from "@/common/utils";
import CarPopup from "../OrderScreens/components/car-popup";
import CoffeeBuyItem from "../OrderScreens/components/coffee-buy-item";
import CoffeeDetailPopup from "../OrderScreens/components/coffee-detail-popup";
import CoffeeListContainer from "../OrderScreens/components/coffee-list-container";
import OrderStepHeader from "../OrderScreens/components/order-header";
import "./index.less";
import ShowShopCar from "../OrderScreens/components/show-shop-car";
import ChooseDiscountCard from "../OrderScreens/components/choose-discount-card";
import {
  carPriceCompute,
  carProductNumCompute,
} from "../OrderScreens/components/car-popup/index.preset";
import { debounce } from "./index.preset";
import RegionSelect from "@/components/region-select";
import OrderRegion from "../OrderScreens/components/order-step-title";

function Coffee() {
  const [regionInfo, setRegionInfo] = useState<LocationList>();
  const [showRegionPop, setShowRegionPop] = useState(false);
  const [showShopCar, setShowShopCar] = useState<boolean>(false);
  const [showCarPopup, steShowCarPopup] = useState(false);
  const [isShowDetailPopup, setIsShowDetailPopup] = useState(false);
  const [currentDetail, setCurrentDetail] = useState({ currentInfo: {} });
  const [coffeeTypeList, setCoffeeTypeList] = useState<CoffeeTypeList[]>([]);
  const [carNum, setCarNum] = useState(0); // 购物车中当前有多少个商品
  const [isSelectedCard, setIsSelectedCard] = useState(false);
  const [priceCompute, setPriceCompute] = useState<calculatePriceRes>();
  const [couponCard, setCouponCard] = useState<{ buy_num: number; use_num: number }>({
    buy_num: 0,
    use_num: 0,
  });
  const [coffeeDataArr, setCoffeeDataArr] = useState([]);
  const [scrollRange, setScrollRange] = useState<any[]>([]);
  const [scrollIndex, setScrollIndex] = useState<number>(0);
  const [scrollRightTop, setScrollRightTop] = useState(0);
  const [scrollHeight, setScrollHeight] = useState<number>(500);
  const [lastItemNum, setLastItemNum] = useState(1);
  const [regionList, setRegionList] = useState<LocationList[]>(getLoad("region_list"));
  
  // 获取咖啡列表
  function getCoffeeTypeFn(reg?: string) {
    getCoffeeTypeList(reg ?? getLoad('region')?.device_id ?? regionList?.[0]?.device_id ?? '').then((res: ResProp<CoffeeTypeList[]>) => {
      const data = res?.Data ?? [];
      setCoffeeTypeList(data);
      const lastNum = 6 - (data?.at(-1) as CoffeeTypeList)?.coffees?.length ?? 1;
      setLastItemNum(lastNum + 1);
    });
  }
  useShareAppMessage(shareIntercept);

  // 计算右边的高度
  function getHeight() {
    return new Promise((resolve) => {
      resolve(1);
    }).then(() => {
      Taro.createSelectorQuery()
        .selectAll("#order-cofee-scroll .right-box")
        .boundingClientRect()
        .exec((res) => {
          console.log(res, "res");
          const data = res?.[0];
          const baseTop = data?.[0]?.top;
          const rangeData = data?.map((item, index) => {
            // const id = coffeeCategoryArr?.[index]?.id;
            const min = item?.top - baseTop;
            const max = min + item?.height + 10;

            // return { id, range: [min, max], index };
            return min;
          });
          console.log(rangeData, "rangeData");

          setScrollRange(rangeData);
        });
    });
  }

  const getDeviceLs = () => {
    const data = getLoad("region_list");
    console.log(data, "data");

    setRegionList(data);
    setShowRegionPop(true);
    setLoad("region", data[0]);
    setRegionInfo(data[0]);
  };
  useEffect(() => {
    getDeviceLs();
    setLoad("shopCar", JSON.stringify({ coffee: [], cup: [], image: [], isChecked: false }));
  }, []);

  async function onScroll(e) {
    if (!scrollRange.length) {
      await getHeight();
    }
    const rightScrollTop = e.detail.scrollTop;
    if (rightScrollTop % 1 === 0) {
      for (let i = 0; i < scrollRange.length; i++) {
        let height1 = scrollRange[i];
        let height2 = scrollRange[i + 1];
        // 如果不存在height2，意味着数据循环已经到了最后一个，设置左边菜单为最后一项即可
        if (!height2 || (rightScrollTop >= height1 && rightScrollTop < height2)) {
          setScrollIndex(i);
          return;
        }
      }
    }
  }

  // 点击左边的品种
  async function clickLeftItem(index) {
    if (!scrollRange.length) {
      await getHeight();
    }
    if (scrollIndex === index) return;
    setScrollIndex(index);
    setScrollRightTop(scrollRange[index]);
  }

  useEffect(() => {
    const shopData = getLoad("shopCar");
    const newData = shopData ? JSON.parse(shopData) : {};
    const coffeeList = newData?.coffee ?? [];
    setCoffeeDataArr(coffeeList);
    const num = carProductNumCompute(coffeeList, setCarNum, isSelectedCard);
    setLoad(
      "shopCar",
      JSON.stringify({
        coffee: coffeeList,
        cup: {},
        image: {},
        isChecked: isSelectedCard,
      })
    );
    // 如果购物车内有商品就去请求接口计算价格
    if (num > 0 && !isShowDetailPopup) {
      // 过滤出咖啡的id
      const computedParams = carPriceCompute(coffeeList, isSelectedCard);
      calculatePriceFn(computedParams);
      setShowShopCar(true);
    }
  }, [isShowDetailPopup, showCarPopup]);

  // 计算购物车价格
  function calculatePriceFn(params) {
    calculatePrice(params).then((res) => {
      setPriceCompute(res as calculatePriceRes);
    });
  }

  function getCouponCardFn() {
    getCouponCard().then((res) => {
      setCouponCard(res);
    });
  }

  useEffect(() => {
    // 计算滚动高度
    const { windowHeight, statusBarHeight = 20 } = Taro.getWindowInfo();
    const height = windowHeight - statusBarHeight;
    setScrollHeight(height);
    getHeight();
    getCouponCardFn();
    getCoffeeTypeFn();
  }, []);

  console.log(showCarPopup || isShowDetailPopup, "asdfsafd");
  const onShowRegionPopup = () => {
    setShowRegionPop(true);
  };
  const onCloseRegionPop = () => {
    setShowRegionPop(false);
  };
  useShareAppMessage(shareIntercept);
  useDidShow(() => {
    const isEnterBuyCoffee =  getLoad('isEnterBuyCoffee')
    if(!isEnterBuyCoffee){
      setShowRegionPop(true);
    }
    setLoad('isEnterBuyCoffee',false)
  });
  const selectRegion = (e, index) => {
    e.stopPropagation();
    setRegionInfo(regionList[index]);
    getCoffeeTypeFn(regionList?.[index]?.device_id)
    setLoad("region", regionList[index]);
  };
  return (
    <View className="order-coffee">
      <NavTopBar />
      <OrderRegion {...regionInfo} onClick={onShowRegionPopup} />
      {/* <OrderStepHeader step={1} title="选咖啡" /> */}
      <View className="coffee-container">
        <ScrollView
          className="left-container"
          scrollY
          scrollTop={0}
          style={{ height: `${scrollHeight - 58}px` }}
        >
          {coffeeTypeList?.map((item, index) => {
            return (
              <View
                key={item.class_id}
                onClick={() => clickLeftItem(index)}
                className={index === scrollIndex ? "active-left left-box" : "left-box"}
              >
                <View className="text">{item.class_name}</View>
              </View>
            );
          })}
        </ScrollView>

        <ScrollView
          id="order-cofee-scroll"
          className="right-container"
          scrollY
          // refresherEnabled
          scrollWithAnimation
          scrollTop={scrollRightTop}
          style={{ height: `${scrollHeight - 58}px` }}
          onScroll={debounce(onScroll, 150)}
        >
          {coffeeTypeList?.map((item) => {
            return (
              <View className="right-box" id={`A${item.class_id}`} key={item.class_id}>
                <CoffeeListContainer title={item.class_name}>
                  {item?.coffees?.map((v) => {
                    return (
                      <CoffeeBuyItem
                        key={v.model_id}
                        data={v}
                        onChooseCoffee={() => {
                          console.log(v, 2232);

                          setIsShowDetailPopup(true);
                          setCurrentDetail({ currentInfo: v });
                        }}
                      />
                    );
                  })}
                </CoffeeListContainer>
              </View>
            );
          })}
          <View
            style={{
              height: `${lastItemNum * 80}px`,
              background: "#f5f5f5",
              color: "#B4B4B4",
              fontSize: "14px",
              textAlign: "center",
              paddingTop: "10px",
            }}
          >
            --- 已到底部 ----
          </View>
        </ScrollView>
      </View>
      <View className="bottomButton">
        {!showShopCar ? (
          <View className="no-choose" onClick={() => setShowShopCar(true)}>
            <Image src={icons.icon_gwd} className="shop-car-img" />
          </View>
        ) : (
          <>
            {couponCard.buy_num === couponCard.use_num && (
              <View className="coupon-container">
                <ChooseDiscountCard
                  isChecked={isSelectedCard}
                  changeFn={(bool) => {
                    setIsSelectedCard(bool);
                    const num = carProductNumCompute(coffeeDataArr, setCarNum, bool);
                    // console.log(coffeeDataArr, "coffeeDataArr--coffeeDataArr");
                    setLoad(
                      "shopCar",
                      JSON.stringify({
                        coffee: coffeeDataArr,
                        cup: {},
                        image: {},
                        isChecked: bool,
                      })
                    );
                    // 如果购物车内有商品就去请求接口计算价格
                    if (num > 0) {
                      // 过滤出咖啡的id
                      const computedParams = carPriceCompute(coffeeDataArr, bool);
                      calculatePriceFn(computedParams);
                    }
                  }}
                />
              </View>
            )}
            <ShowShopCar
              priceData={priceCompute as calculatePriceRes}
              onShowCar={() => steShowCarPopup(true)}
              carNum={carNum}
              checkedCard={isSelectedCard}
              toSettlementFn={() => {
                // 这个地方后面有改动，看看设置的过滤出设置的值为true的

                Taro.navigateTo({
                  url: "/pages/OrderScreens/buycoffee/index",
                });
              }}
            />
          </>
        )}
      </View>
      <PageContainer
        show={showCarPopup || isShowDetailPopup || showRegionPop}
        position="bottom"
        onClickOverlay={() => setShowRegionPop(false)}
        // closeOnSlideDown={true}
        zIndex={isShowDetailPopup ? 2000 : 100}
        onAfterEnter={() => setShowShopCar(false)}
        round
        className="showCarPageContainer"
        style={{
          backgroundColor: "rgba(0,0,0,0.3)",
        }}
        onAfterLeave={() => {
          setIsShowDetailPopup(false);
          steShowCarPopup(false);
        }}
      >
        <PopUpHeader
          onClick={() => {
            setIsShowDetailPopup(false);
            steShowCarPopup(false);
            setIsShowDetailPopup(false);
          }}
        />
        {showCarPopup && (
          <CarPopup
            couponCard={couponCard}
            isChecked={isSelectedCard}
            changeCardFn={(bool) => setIsSelectedCard(bool)}
          />
        )}
        {isShowDetailPopup && (
          <CoffeeDetailPopup
            onClose={() => setIsShowDetailPopup(false)}
            detail={currentDetail?.currentInfo}
          />
        )}
        {showRegionPop && (
          <RegionSelect
            regionList={regionList}
            selectRegion={selectRegion}
            onClose={onCloseRegionPop}
            regionInfo={regionInfo}
          />
        )}
      </PageContainer>
    </View>
  );
}

export default Coffee;
