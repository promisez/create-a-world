import icons from "@/common/icon";

export const defaultUrl =
  "https://img95.699pic.com/element/40110/8809.png_300.png!/fw/431/clip/0x300a0a0";

export const cardList = [
  {
    url: icons.icon_dd,
    label: "咖啡订单",
    to: "/pages/MeScreens/orderlist/index",
    expect: false,
    tag: 'coffee'
  },
  {
    url: icons.icon_sp,
    label: "商品订单",
    to: "/pages/MeScreens/orderlist/index",
    expect: false,
    tag: 'product'
  },
  {
    url: icons.icon_sy,
    label: "创作收益",
    to: "/pages/MeScreens/orderlist/index",
    expect: true,
    tag: 'income'
  },
  {
    url: icons.icon_yhq,
    label: "优惠券",
    to: "/pages/OrderScreens/favorable/index?onlyWatch=1",
    expect: false,
    tag: ''
  },
];

export const list = [
  // {
  //   title: '我的发布',
  //   url: '/pages/MeScreens/published/index',
  //   thumb: icons.icon_fabu
  // },
  // {
  //   title: '我的收藏',
  //   url: '/pages/my/collected/index',
  //   thumb: icons.icon_shoucang
  // },
  // {
  //   title: '批量定制杯子礼品',
  //   url: 'https://www.baidu.com',
  //   thumb: icons.icon_dingzhi
  // },
  {
    title: "隐私政策",
    url: "privacy_policy",
    thumb: icons.icon_yinsi,
  },
  {
    title: "用户服务协议",
    url: "user_policy",
    thumb: icons.icon_xieyi,
  },
  {
    title: "招商加盟",
    url: "https://www.baidu.com",
    thumb: icons.icon_jiameng,
  },
  {
    title: "联系客服",
    url: "https://www.baidu.com",
    thumb: icons.icon_kefu,
  },
];
