import NavTopBar from "@/components/nav-bar";
import { Button, Image, Input, Text, View } from "@tarojs/components";
import Taro, { useShareAppMessage } from "@tarojs/taro";
import { useEffect, useState } from "react";
import "./index.less";
import { Arrow } from "@taroify/icons";
import { cardList, defaultUrl, list } from "./index.preset";
import { getToken } from "@/api/my";
import icons from "@/common/icon";
// import CountCard from "../order/components/count-card";
import { getLoad, openPDF, setLoad, shareIntercept } from "@/common/utils";
import CountCard from "../OrderScreens/components/count-card";
import { getMobile } from "@/api/home";
import { toast } from "@/api/util";

function My() {
  const [avatar, setAvatar] = useState(defaultUrl);
  useShareAppMessage(shareIntercept);

  const onChooseAvatar = (e) => {
    console.log(e, "onChooseAvatar");
    // 请求upload接口
    setLoad("avatar", e.detail.avatarUrl);
    setAvatar(e.detail.avatarUrl);
  };

  const onAtClick = (item) => {
    if (["privacy_policy", "user_policy"].includes(item.url)) {
      const configObj = {
        privacy_policy: "隐私协议",
        user_policy: "用户协议",
      };
      const commonInfo = getLoad("common_info");
      const url = commonInfo[item.url];
      const title = configObj[item.url];
      openPDF(url, title);
      return;
    }
    Taro.navigateTo({
      url: item.url,
    });
  };
  return (
    <View className="meContainer">
      <View className="avatarContainer">
        <View className="userInfoContent">
          <View className="userInfo">
            <Button
              className="avatarButton"
              openType="chooseAvatar"
              onChooseAvatar={onChooseAvatar}
            >
              <Image className="avatar" src={avatar}></Image>
            </Button>
            <View className="userInfoText">
              <Text type="nickname" className="nickname">
                K.O用户
              </Text>
              <Text className="phoneNum">{getLoad("phoneNum").replace(/(\d{3})\d{4}(\d{4})/, "$1****$2")}</Text>
            </View>
          </View>
          <View className="iconContent">
            {/* <Image className="setImage" src={icons.icon_btn_set}></Image> */}
          </View>
        </View>
        <View className="myCardFiexd">
          <Image
            className="vipCard"
            src={icons.to_bout_card}
            onClick={() => {
              Taro.navigateTo({
                url: "/pages/MeScreens/cardRule/index",
              });
            }}
          />
        </View>
      </View>
      <View className="meHCardList">
        {cardList.map((item, index) => (
          <View
            key={index}
            className="meHCardItem"
            onClick={(e) => {
              if (["income", "product"].includes(item.tag)) {
                e.stopPropagation();
                Taro.showToast({
                  title: "功能准备上线，敬请期待",
                  icon: "none",
                });
                return;
              }
              Taro.navigateTo({
                url: item.to,
              });
            }}
          >
            <Image className="meHCardImage" src={item?.url}></Image>
            <Text className={item.expect ? "meHCardText expect" : "meHCardText"}>
              {item?.label}
            </Text>
            <View className="expectation">{item.expect ? "(敬请期待)" : ""}</View>
          </View>
        ))}
      </View>
      <View className="meList">
        {list.map((item, index) => (
          <View key={index} className="meListItem" onClick={() => onAtClick(item)}>
            <View className="meListContent">
              <Image className="meListImage" src={item.thumb}></Image>
              <Text className="meListText">{item.title}</Text>
            </View>
            <View className="meListArrow">
              <Arrow size={12} color="#B4B4B4" />
            </View>
          </View>
        ))}
      </View>
      <Button
        openType="getPhoneNumber"
        onGetPhoneNumber={(res) => {
          console.log(res, "获取手机号");
          const { code, encryptedData, iv, errMsg } = res.detail;
          if (errMsg === "getPhoneNumber:ok") {
            getMobile({
              code,
              encryptedData,
              iv,
            })
              .then((res) => {
                setLoad("phoneNum", res?.mobile);
              })
              .catch((err) => {
                console.log(err, "获取手机号报错了");
                toast(err);
              });
          } else {
            setLoad("phoneNum", "");
          }
        }}
        className="phoneButton"
      >
        获取手机号领取优惠券
      </Button>
    </View>
  );
}

export default My;
