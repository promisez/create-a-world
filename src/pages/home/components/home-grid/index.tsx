import { View, Image } from "@tarojs/components";
import Taro from "@tarojs/taro";
import icon from "@/common/icon";
import React from "react";
import "./index.less";

const arr = [
  {
    image: icon.icon_yaoqing,
    value: "邀请有礼",
    to: "/pages/HomeScreens/invite/index",
    expect: false,
    tag: "",
  },
  // {
  //   image: icon.icon_zhoubian,
  //   value: "周边商城",
  //   to: "",
  //   expect: false,
  //   tag: "mall",
  // },
  {
    image: icon.icon_fuli_default,
    value: "创作福利",
    to: "",
    expect: true,
    tag: "welfare",
  },
  {
    image: icon.icon_shequn,
    value: "咖友社群",
    to: "/pages/HomeScreens/association/index",
    expect: false,
    tag: "",
  },
];

function HomeGrid() {
  return (
    <View className="home-grid-container">
      {arr?.map((item, index) => {
        return (
          <View
            key={index}
            className="grid-item"
            onClick={(e) => {
              if (["mall", "welfare"].includes(item.tag)) {
                e.stopPropagation();
                Taro.showToast({
                  title: "功能准备上线，敬请期待",
                  icon: "none",
                });
                return;
              }
              Taro.navigateTo({
                url: item.to,
              });
            }}
          >
            <Image src={item.image} className="grid-item-img" />
            <View className={item.expect ? "grid-item-text expect" : "grid-item-text"}>
              {item.value}
            </View>
            <View className="expectation">{item.expect ? "(敬请期待)" : ""}</View>
          </View>
        );
      })}
    </View>
  );
}

export default HomeGrid;
