import { View, Image } from "@tarojs/components";
import React from "react";
import "./index.less";


function HomeImageText(props) {
  const { url, onClick } = props;
  return (
    <View className="home-image-text" onClick={onClick}>
      <Image src={url} className="background-image" />
    </View>
  );
}

export default HomeImageText;
