import { Button, Image, Swiper, SwiperItem, Text, View } from "@tarojs/components";
import Taro, { useLoad, useRouter, useShareAppMessage } from "@tarojs/taro";
import { useEffect, useState } from "react";
import HomeGrid from "./components/home-grid";
import HomeImageText from "./components/home-image-text";
import "./index.less";
import { Dialog } from "@taroify/core";
import { dialogcontent } from "./index.preset";
import { getBanner, getCommonInfo, getCoupon, getMobile } from "@/api/home";
import { getToken } from "@/api/my";
import icon from "@/common/icon";
import React from "react";
import { WebView } from "@tarojs/components";
import icons from "@/common/icon";
import { displayShare, getLoad, openPDF, setLoad, shareIntercept } from "@/common/utils";
import { toast } from "../../api/util";
import { getDeviceList } from "@/api/order";

const defaultUrl = "https://img95.699pic.com/element/40110/8809.png_300.png!/fw/431/clip/0x300a0a0";
export default function Index() {
  const [actIndex, setActIndex] = useState(0);
  const [open, setOpen] = useState(false);
  const [couponOpen, setCouponOpen] = useState(false);
  const [couponList, setCouponList] = useState<any[]>([]);
  const [commonInfo, setCommonInfo] = useState<any>({});
  const [bannerList, setBannerList] = useState<string[]>([]);
  const avatarUrl = getLoad("avatar") ?? defaultUrl;
  useShareAppMessage(shareIntercept);
  const router = useRouter();
  const init = async () => {
    const res = await getCommonInfo();
    console.log(res, "resres");
    setCommonInfo(res);
    setLoad("common_info", res);
    const isAgree = getLoad("privacy_agree");

    if (!isAgree) {
      setOpen(true);
      return;
    }
    loginFn();
  };
  const loginFn = async () => {
    if (!getLoad("token")) {
      const resLogin = await Taro.login();
      const shareToken = router.params?.token;
      await getToken(resLogin.code ?? "", shareToken);
    }
    // 领优惠券
    const resCoupon = await getCoupon();
    if (!resCoupon || resCoupon.length === 0) return;
    setCouponList(resCoupon);
    setCouponOpen(true);
  };
  useLoad(() => {
    console.log("Page loaded.");

    init();
  });
  const onAgree = () => {
    setOpen(false);
    loginFn();
    setLoad("privacy_agree", true);
  };
  const onNotAgree = () => {
    setOpen(false);
    Taro.exitMiniProgram();
  };
  const openDoc = (e, type: "privacy_policy" | "user_policy") => {
    e.stopPropagation();
    const configObj = {
      privacy_policy: "隐私协议",
      user_policy: "用户协议",
    };
    const url = commonInfo[type];
    const title = configObj[type];
    openPDF(url, title);
  };
  const couponBtnFn = (e) => {
    e.stopPropagation();
    setCouponOpen(false);
    Taro.navigateTo({
      url: "/pages/OrderScreens/favorable/index?onlyWatch=1",
    });
  };
  const onClearStorage = () => {
    Taro.clearStorageSync();
    toast("清除成功，点击右上角三个点，选择重新进入");
  };
  function getBannerFn() {
    getBanner().then((res) => {
      const { banners } = res;
      setBannerList(banners ?? []);
    });
  }
  useEffect(() => {
    Taro.getLocation({
      type: "wgs84",
      success: function (res) {
        const { latitude, longitude } = res;
        getDeviceList({ longitude, latitude }).then((res) => {
          setLoad("region_list", res);
        });
        setLoad("currentRegion", { latitude, longitude });
      },
      fail: function () {
        toast("获取位置失败");
      },
    });
    const currentRegion = getLoad("currentRegion");
    if (currentRegion) {
      getDeviceList(currentRegion).then((res) => {
        setLoad("region_list", res);
      });
    }
    getBannerFn();
  }, []);
  return (
    <View className="home-container">
      <View className="swiperView">
        <Swiper
          className="home-swiper"
          circular
          autoplay
          onChange={(e) => setActIndex(e.detail.current)}
        >
          {bannerList?.map((item, index) => {
            return (
              <SwiperItem key={index} className="home-swiper-item">
                <Image src={item} className="home-swiper-item-image" />
              </SwiperItem>
            );
          })}
        </Swiper>
        <View className="legend">
          {bannerList?.map((item, index) => {
            return <View key={index} className={`circle ${actIndex === index ? "active" : ""}`} />;
          })}
        </View>
        <View className="home-user-container">
          <View className="user">
            <Image src={avatarUrl} className="img" />
            <Text className="nickname">K.O用户</Text>
          </View>
          <View className="income">
            <View
              className="money"
              onClick={() => {
                Taro.navigateTo({
                  url: "/pages/MeScreens/orderlist/index",
                });
              }}
            >
              咖啡订单 {" >"}
            </View>
          </View>
        </View>
      </View>
      <View className="home-show-container">
        <View className="home-click-image">
          <HomeImageText
            url={icon.icon_order_entry}
            onClick={() => Taro.switchTab({ url: "/pages/coffee/index" })}
          />
          <HomeImageText
            url={icon.img_chatroom}
            onClick={() => {
              Taro.navigateTo({ url: "/pages/HomeScreens/chatRoomList/index" });
            }}
          />
        </View>
        <View className="surrounding-content">
          <HomeGrid />
        </View>
        <View className="other-container">
          <Image src={icon.icon_poster} className="img" />
        </View>
        {/* <View onClick={onLogin} className='clearBtnText'>登录授权</View> */}
        {/* <View>
          <Button
            openType="getPhoneNumber"
            onGetPhoneNumber={(res) => {
              console.log(res, "onGetPhoneNumber");

              const { code, encryptedData, iv, errMsg } = res.detail
              if (errMsg === 'getPhoneNumber:ok') {
                getMobile({
                  code,
                  encryptedData,
                  iv
                })
              }
            }}
          >
            获取手机号
          </Button>
        </View> */}
        {/* <View onClick={onClearStorage} className="clearBtnText">
          点击清除缓存 1.1.22
        </View> */}
      </View>
      {/* 隐私协议 */}
      <Dialog className="home-dialog" open={open}>
        <Dialog.Header className="dialog-header">K·O咖啡隐私与协议</Dialog.Header>
        <Dialog.Content align="left">
          感谢您选择K·O咖啡！我们十分注重用户的隐私保护和个人信息保护。在您使用K·O咖小程序前请认真阅读
          <Text className="blue-active" onClick={(e) => openDoc(e, "user_policy")}>
            《K·O咖啡小程序的用户协议》
          </Text>
          与
          <Text className="blue-active" onClick={(e) => openDoc(e, "privacy_policy")}>
            《K·O咖啡隐私协议》
          </Text>
          全部条款，您同意并接受全部条款后可开始我们的全部服务。
          <View className="dialog-footer">
            <View className="dialog-agree" onClick={onAgree}>
              同意并继续
            </View>
            <View className="dialog-refuse" onClick={onNotAgree}>
              不同意，退出
            </View>
          </View>
        </Dialog.Content>
      </Dialog>
      {couponOpen && (
        <View className="couponDialog" onClick={() => setCouponOpen(false)}>
          <View className="couponBox">
            <Image className="couponImg" src={icons.coupon_dialog}></Image>
            <View className="couponList">
              {couponList.map((item, index) => (
                <View className="couponItem" key={index}>
                  <Image className="couponIcon" src={icons.youhuiquan}></Image>
                  {item?.discount && (
                    <View className="couponValue">
                      <Text className="couponNumber">{item?.discount}</Text>
                      <Text>折</Text>
                    </View>
                  )}
                  <View className="couponContent">
                    <Text className="couponTitle">优惠券</Text>
                    <Text className="counponTip">{item?.name}</Text>
                  </View>
                </View>
              ))}
            </View>
            <View className="couponBtnBox">
              <View className="couponBtn" onClick={couponBtnFn}>
                立即领取
              </View>
            </View>
          </View>
        </View>
      )}
    </View>
  );
}
