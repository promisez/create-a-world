import { View, Image } from "@tarojs/components";
import React from "react";
import "./index.less";

const coffeeStateMap = {
  0: '排队',
  1: '制作中',
  2: '制作中',
  3: '待取餐',
  4: '已完成',
}

function MyOrderListItem(props) {
  const { logoUrl, name, sweet, ice, num, clickFn, state } = props;

  return (
    <View className="orderListItemComtainer" onClick={clickFn}>
      <View className="productItem">
        <Image src={logoUrl} className="itemImg" />
        <View className="itemInfo">
          <View className="name">{name}</View>
          <View className="chooseInfo">{`${ice}+${sweet}`}</View>
          <View className="chooseInfo">{coffeeStateMap?.[state]}</View>
        </View>
        <View className="itemPrice">
          <View className="num">x {num}</View>
        </View>
      </View>
    </View>
  );
}

export default MyOrderListItem;
