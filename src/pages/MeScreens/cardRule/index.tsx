import Header from "@/components/common-header";
import NavTopBar from "@/components/nav-bar";
import { View, Text, Image } from "@tarojs/components";
import exp from "constants";
import React, { useEffect, useState } from "react";
import "./index.less";
import Taro, { useDidShow } from "@tarojs/taro";
import { getLoad, setLoad } from "@/common/utils";
import { getCouponCard } from "@/api/coffee";
import icon from "@/common/icon";
const CardRule = () => {
  const [couponCard, setCouponCard] = useState<{ buy_num: number; use_num: number }>({
    buy_num: 0,
    use_num: 0,
  });

  const goBuy = (e) => {
    if (couponCard.buy_num !== couponCard.use_num) {
      e.stopPropagation();
      return;
    }
    Taro.switchTab({
      url: "/pages/coffee/index",
    });
  };
  function getCouponCardFn() {
    getCouponCard().then((res) => {
      setCouponCard(res);
    });
  }
  const coupon_card = getLoad("common_info").coupon_card;
  console.log(coupon_card, "coupon_card==coupon_card");
  useDidShow(() => {
    setLoad("isEnterBuyCoffee", true);
  });
  useEffect(() => {
    getCouponCardFn();
  }, []);

  const list = [
    "可使用10次，所有饮品使用优惠卡购买",
    "优惠卡自购买日起有效期30天",
    "本优惠卡全国通用",
  ];
  return (
    <View className="cardRulePage">
      <NavTopBar />
      <Header title="次卡优惠规则" />
      <View className="cardBox" >
        <Image src={icon.bout_card} className="koBox" />
        
        <View className="cardRule">
          <View className="cardTitle">优惠规则</View>
          <View>
            {list.map((item, index) => {
              return (
                <View className="cardTip" key={index}>
                  {index + 1}、{item}
                </View>
              );
            })}
          </View>
          <View className="cardBtnBox">
            <View className="cardBtntext" onClick={coupon_card}>
              仅售<Text className="cardBtnNum">15</Text>元
            </View>
            <View className="btnBox">
              <View
                className={
                  couponCard.buy_num === couponCard.use_num ? "cardBtn" : "cardBtn cardBtnDisabled"
                }
                onClick={goBuy}
              >
                {couponCard.buy_num === couponCard.use_num ? "立即购买" : "已购买"}
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};
export default CardRule;
