import NavTopBar from "@/components/nav-bar";
import { View, Image } from "@tarojs/components";
import React from "react";
import Headers from "@/components/common-header";
import Taro from "@tarojs/taro";
import icons from "@/common/icon";
import "./index.less";

function Published() {
  return (
    <View className="published_container">
      <NavTopBar />
      <Headers title="我的发布" />
      <View className="published_content">已发布的内容</View>

      <View className="bottomButton">
        <View className="no-choose" onClick={() => Taro.switchTab({ url: "/pages/mall/index" })}>
          <Image src={icons.icon_gwd} className="shop-car-img" />
        </View>
      </View>
    </View>
  );
}

export default Published;
