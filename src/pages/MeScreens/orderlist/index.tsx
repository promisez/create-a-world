import NavTopBar from "@/components/nav-bar";
import { View, ScrollView } from "@tarojs/components";
import React, { useEffect, useState } from "react";
import CommonHeaders from "@/components/common-header";
import CommonOrderNum from "@/components/common-order-num";
import Taro, { useDidShow, useReachBottom, useShareAppMessage } from "@tarojs/taro";
import { setLoad, shareIntercept, showTimeFn } from "@/common/utils";
import { getMyOrderList, orderListRes } from "@/api/my-order-list";
import MyOrderListItem from "../components/order-item";
import "./index.less";
import { carPriceCompute } from "@/pages/OrderScreens/components/car-popup/index.preset";

function MyOrderList() {
  useShareAppMessage(shareIntercept);
  const [orderList, setOrderList] = useState<orderListRes[]>([]);
  const [pageIndex, setPageIndex] = useState(1);
  const [loading, setLoading] = useState(false);
  function getOrderListFn(i) {
    getMyOrderList(i)
      .then((res) => {
        console.log(res, "获取我的订单列表");
        if (pageIndex === 1) {
          setOrderList(res ?? []);
        } else {
          setOrderList((r) => r?.concat(res ?? []));
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }

  useEffect(() => {
    if (pageIndex !== 1) {
      getOrderListFn(pageIndex);
    }
  }, [pageIndex]);

  useDidShow(() => {
    Taro.setStorageSync(
      "shopCar",
      JSON.stringify({
        coffee: [],
        cup: {},
        image: {},
        isChecked: false,
      })
    );
    getOrderListFn(1);
  });

  useReachBottom(() => {
    console.log("上拉刷新页面时触发");
  });
  return (
    <View className="myOrderListContainer">
      <NavTopBar />
      <CommonHeaders title="咖啡订单" />
      <ScrollView
        scrollY
        scrollWithAnimation
        className="produceListItem"
        refresherEnabled
        refresherTriggered={loading}
        onRefresherRefresh={() => {
          setLoading(true);
          setPageIndex(1);
          getOrderListFn(1);
          Taro.showToast({
            title: "获取成功",
          });
        }}
        onScrollToLower={() => setPageIndex((res) => res + 1)}
      >
        {orderList?.map((item, index) => {
          const { TakeInfo, DisplayState, OrderTime, PayInfo, MakeInfo, TakeNum, MakeState } = item;
          const { List, card_add, final_price } = PayInfo;
          const time = showTimeFn(OrderTime * 1000);
          let num = List?.length;
          if (card_add === 10) {
            num += 1;
          }
          const { Coffees } = MakeState;
          return (
            <View className="itemContent" key={index}>
              <CommonOrderNum
                orderNumber={TakeInfo?.order_id}
                orderState={DisplayState}
                coffeeNum={Coffees?.length}
                TakeNum={TakeNum}
              />
              {Coffees?.map((k) => {
              const {make_info, state} = k
                return (
                  <MyOrderListItem
                    logoUrl={make_info?.coffee?.pic}
                    name={make_info?.coffee?.name}
                    sweet={make_info?.sweet}
                    ice={make_info?.cool}
                    num={num ?? "0"}
                    state={state}
                    clickFn={() => {
                      Taro.navigateTo({
                        url: `/pages/OrderScreens/orderdetail/index?order_id=${TakeInfo.order_id}`,
                      });
                    }}
                  />
                );
              })}

              <View className="showTimePrice">
                <View className="time">{time}</View>
                <View className="price">合计 ￥{final_price ?? "0"}</View>
              </View>
              <View className="action">
                <View
                  className="anotherOrder"
                  onClick={() => {
                    // 过滤出咖啡需要存储的信息
                    const newData = List?.map((v) => {
                      return {
                        ...v.coffee,
                        sweet: v.sweet,
                        ice: v.cool,
                      };
                    });
                    const coffeeList = carPriceCompute(newData, false);
                    Taro.setStorageSync(
                      "shopCar",
                      JSON.stringify({
                        coffee: newData,
                        cup: {},
                        image: {},
                        isChecked: false,
                      })
                    );
                    setLoad("shop-payload", coffeeList);
                    setOrderList([]);
                    Taro.navigateTo({
                      url: "/pages/OrderScreens/buycoffee/index?device_id=" + MakeInfo.device_id,
                    });
                  }}
                >
                  再来一单
                </View>
              </View>
            </View>
          );
        })}
        <View className="noMore">已经到底了</View>
      </ScrollView>
    </View>
  );
}

export default MyOrderList;
