import NavTopBar from "@/components/nav-bar";
import { View, Image } from "@tarojs/components";
import Taro from "@tarojs/taro";
import React from "react";
import Headers from "@/components/common-header";
import icons from "@/common/icon";

import './index.less'

function Collected() {
  return (
    <View className="collected_container">
      <NavTopBar />
      <Headers title="我的收藏" />
      <View className="collected_content">已收藏的内容</View>
    </View>
  );
}

export default Collected;
