import icons from "@/common/icon";

export const stepList = [
  {
    url: icons.icon_share_logo,
    text: '分享链接',
    subText: '给好友'
  },
  {
    url: icons.icon_invite_coupon,
    text: '好友注册',
    subText: '并获得折扣券'
  },
  {
    url: icons.icon_invite_complete,
    text: '好友完成',
    subText: '首单'
  },
  {
    url: icons.icon_invite_rewards,
    text: '邀请达成',
    subText: '获得奖励'
  },
]