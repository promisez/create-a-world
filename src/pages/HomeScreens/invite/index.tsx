import { shareIntercept } from "@/common/utils";
import Header from "@/components/common-header";
import NavTopBar from "@/components/nav-bar";
import { View, Image, Text, Button } from "@tarojs/components";
import { useShareAppMessage } from "@tarojs/taro";
import { useCallback, useEffect } from "react";
import "./index.less";
import icons from "@/common/icon";
import { stepList } from "./index.preset";

function Invite() {
  useShareAppMessage(shareIntercept);
  const IconItem = useCallback(({ url, text, subText, showArrow }) => (
    <View className='iconBox'>
      <View className='imageBox' style={{ marginRight: showArrow ? '28px' : '0' }}>
        <Image className='iconImage' src={url}></Image>
        <View className='textBox'>
          <View className='iconTip'>{text}</View>
          <View className='iconTip'>{subText}</View>
        </View>
      </View>
      {showArrow && <Image className='iconImageRight' src={icons.icon_arrow_right_fill}></Image>}


    </View>
  ), [])
  return (
    <View className="invitePage">
      <NavTopBar />
      <Header title="邀请有礼" />
      <View className="koContent">
        <Image className="koImage" src={icons.icon_ko_new_bg} mode='aspectFit'></Image>
      </View>
      {/* <View className='inviteBtnBox'>
        <Button openType='share' className='inviteBtn'>
          <Image className='wxIcon' src={icons.icon_wx_logo}></Image>
          <Text className='btnText'>邀请好友</Text>
        </Button>
      </View> */}
    </View>
  );
}

export default Invite;
