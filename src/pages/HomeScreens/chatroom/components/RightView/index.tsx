import { Avatar } from "@taroify/core";
import { View, Text } from "@tarojs/components";
import "./index.less";

const RightView = (props) => {
  const { content, from, showTooltip, onLongPress, onLongPressAvatar } = props;
  return (
    <View
      className="rightView rightViewBox"
      onLongPress={(e) => {
        console.log(e, "长按以下");
      }}
    >
      <View className="msgBox">
        <View className="peopleName">{from.nick}</View>
        <View
          className="contentText"
          onLongPress={(e) => {
            console.log(e, "长按以下");
          }}
        >
          {content}
        </View>
      </View>
      <Avatar
        onLongPress={(e) => {
          console.log(e, "长按以下");
        }}
        src="https://joesch.moe/api/v1/random"
        size="mini"
      />
    </View>
  );
};

export default RightView;
