import { View } from "@tarojs/components";
import React, { useState } from "react";
import './index.less'
function TooltipView(props) {
  const { className, text, showTooltip, onLongPress } = props;
  const onReply = (e) => {
    e.stopPropagation();
    console.log('回复')
  }
  const onCopy = (e) => {
    e.stopPropagation();
    console.log('复制');

  }
  return (
    <View className="tooltipView">
      <View className={className} onLongPress={onLongPress}>
        {text}
      </View>
      {showTooltip && (
        <View className="tooltipViewMenu">
          <View className="copy" onClick={onCopy} >复制</View>
          <View className="reply" onClick={onReply} >回复</View>
        </View>
      )}
    </View>
  );
}

export default TooltipView;
