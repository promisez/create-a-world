import { View, Text } from "@tarojs/components";
import { Avatar } from "@taroify/core";
import "./index.less";
import TooltipView from "../tooltip-view";

const LeftView = (props) => {
  const { content, from, showTooltip, onLongPress, onLongPressAvatar } = props;
  return (
    <View className="leftView leftViewBox">
      <Avatar onLongPress={onLongPressAvatar} src="https://joesch.moe/api/v1/random" size="mini" />
      <View className="msgBox">
        <View className="peopleName">{from.nick}</View>
        <TooltipView
          className="contentText"
          text={content}
          showTooltip={showTooltip}
          onLongPress={onLongPress}
        />
      </View>
    </View>
  );
};

export default LeftView;
