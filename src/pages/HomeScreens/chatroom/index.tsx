import { Input, View, Text, ScrollView, Form, Textarea, Label } from "@tarojs/components";
import React, { useEffect, useRef, useState } from "react";
import NavTopBar from "@/components/nav-bar";
import CommonHeader from "@/components/common-header";
import "./index.less";
import Taro from "@tarojs/taro";
import Button from "@taroify/core/button/button";
import LeftView from "./components/LeftView";
import RightView from "./components/RightView";
import { msgAction, socketMessage } from "./socket";
import { toast } from "@/api/util";
import { Search } from "@taroify/core";
import { getLoad } from "@/common/utils";

const d_arr = [
  { name: "测试人员", left: false, text: "12312" },
  { name: "测试人员", left: true, text: "不是 12312" },
  { name: "测试人员", left: false, text: "12121" },
  { name: "测试人员", left: true, text: "不是 12121" },
  { name: "测试人员", left: false, text: "ewewe" },
  { name: "测试人员", left: true, text: "不是 ewewe" },
  { name: "测试人员", left: false, text: "ewewe" },
  { name: "测试人员", left: true, text: "不是 ewewe" },
  { name: "测试人员", left: false, text: "ewewe" },
  { name: "测试人员", left: true, text: "不是 ewewe" },
  { name: "测试人员", left: false, text: "ewewewew" },
  { name: "测试人员", left: true, text: "不是 ewewewew" },
  { name: "测试人员", left: false, text: "dsdssd" },
  { name: "测试人员", left: true, text: "不是 dsdssd" },
  { name: "测试人员", left: false, text: "fdfd" },
  { left: true, text: "不是 fdfd" },
];

let currentUserId = "";
function ChatRoom() {
  const [messageList, setMessageList] = useState<any[]>([]);
  const [scrollTop, setScrollTop] = useState<number>(10000);
  const [isLogin, setIsLogin] = useState(false);
  const roomId = new URLSearchParams(location.search?.slice(1))?.get("roomId") ?? "";
  const formRef = useRef<any>(null);
  const [currentMsg, setCurrentMsg] = useState<string>("");
  const [showTooltip, setShowTooltip] = useState<boolean[]>([]);
  const currentregion = getLoad("currentRegion");
  const region = {
    latitude: String(currentregion?.latitude) ?? "",
    longitude: String(currentregion?.longitude) ?? "",
  };
  const ref = useRef<any>();
  const inpRef = useRef<any>();

  function getMessageFn() {
    ref.current = Taro.connectSocket({
      url: "wss://www.kocoffee.top/ws/",
    }).then((task) => {
      task.onOpen(function () {
        console.log("onOpen");
        const joinRoom = JSON.stringify({
          path: "JoinRoom",
          room_id: roomId ?? "",
          local: region,
        });
        task.send({ data: joinRoom });
        task.onMessage((msg) => {
          const newMsg = JSON.parse(msg?.data);
          const obj = msgAction(newMsg);
          console.log(newMsg, obj, "接受消息");
          if (!!obj?.LoginRsp?.id) {
            currentUserId = obj?.LoginRsp?.id;
          }
          if (obj?.RoomMsgPush) {
            if (obj.RoomMsgPush.from.id === currentUserId) {
              setMessageList((res) => [...res, { ...obj?.RoomMsgPush, left: false }]);
            } else {
              setMessageList((res) => [...res, { ...obj?.RoomMsgPush, left: true }]);
            }
          }
        });
      });
    });
  }

  useEffect(() => {
    getMessageFn();
    Taro.onSocketClose(function (res) {
      console.log("WebSocket 已关闭！");
    });
    return () => {
      Taro.closeSocket();
    };
  }, []);

  // 生成消息格式
  const generateMsg = (text) => {
    return JSON.stringify({
      path: "SendMsg",
      content: text,
      room_id: roomId ?? "", // 地址栏获取
      local: region,
    });
  };
  const sendMsg = (value) => {
    console.log(value, "发消息中...");
    Taro.sendSocketMessage({
      data: value,
      success(res) {
        console.log(res, "发送成功");
      },
      fail(err) {
        console.log(err, "发送失败");
      },
      complete(res) {
        setCurrentMsg("");
      },
    });
  };
  // 第一次发消息
  const onSendFirst = (value) => {
    const token = getLoad("token");
    const avatar = getLoad("avatar");
    const loginRoom = JSON.stringify({
      path: "Login",
      local: region,
      token,
      face: avatar,
    });
    console.log("登录中...");
    Taro.sendSocketMessage({
      data: loginRoom,
      success(res) {
        console.log(res, "登录成功");
        setIsLogin(true);
        // 发送第一条消息
        sendMsg(value);
      },
      fail(err) {
        console.log(err, "登录失败");
      },
    });
  };
  const onSend = (val) => {
    console.log("发送中...");
    if (!isLogin) {
      onSendFirst(generateMsg(val));
      return;
    }
    console.log("已登录房间，发送消息...");
    sendMsg(generateMsg(val));
  };

  useEffect(() => {
    setScrollTop(scrollTop + 100);
    setShowTooltip(messageList.map((_) => false));
  }, [messageList]);

  const onLongPress = (index) => {
    const newState = JSON.parse(JSON.stringify(showTooltip));
    newState[index] = true;
    console.log("长按");

    setShowTooltip(newState);
  };

  const onLongPressAvatar = (info) => {
    console.log(info, "onLongPressAvatar");
    setCurrentMsg(currentMsg + "@" + info.name + " ");
  };

  useEffect(() => {}, []);

  return (
    <View
      className="homeChatRoomContainer"
      onClick={() => {
        console.log("点击元素以外的地方");
        setShowTooltip(messageList.map((_) => false));
      }}
    >
      <NavTopBar />
      <CommonHeader title="创小鹰聊天室" />
      <ScrollView scrollY scrollTop={scrollTop} className="contentBox">
        {messageList.map((v, i) => {
          return v?.left ? (
            <LeftView
              {...v}
              key={i}
              showTooltip={showTooltip?.[i]}
              onLongPress={() => onLongPress(i)}
              onLongPressAvatar={() => onLongPressAvatar(v)}
            />
          ) : (
            <RightView {...v} key={i} />
          );
        })}
      </ScrollView>
      <Form
        ref={formRef}
        onSubmit={(e) => {
          console.log(e, formRef, "点击发送");
          const { text } = e.detail.value ?? { text: "" };
          if (!text.trim().length) {
            toast("消息不能为空");
            setCurrentMsg("");
            return;
          }
          inpRef.current.focus();
          onSend(text);
        }}
      >
        <View className="iptBox">
          {/* <Textarea value={''}></Textarea> shou */}
          {/* <Search
            value={}
            placeholder="请输入搜索关键词"
            action
            onChange={(e) => setValue(e.detail.value)}
            onCancel={() => setOpen(true)}
          /> */}
          <Input
            id="btn"
            name="text"
            className="textIuput"
            type="text"
            ref={inpRef}
            value={currentMsg}
            onInput={(e) => setCurrentMsg(e?.detail?.value ?? "")}
            adjustPosition={false}
            focus={true}
            onKeyboardHeightChange={(e) => {
              const screenInfo = Taro.getSystemInfoSync();
              console.log(screenInfo.windowHeight, "windowHeight");
              const box = document.getElementsByClassName("homeChatRoomContainer")[0];
              box.style.height = screenInfo.windowHeight - e.target?.height + "px";
              console.log(box, "contentBox");
            }}
          />
          <Button size="mini" className="sendBtn" formType="submit">
            发送
          </Button>
        </View>
      </Form>
    </View>
  );
}

export default ChatRoom;
