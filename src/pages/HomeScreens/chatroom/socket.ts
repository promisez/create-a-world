import Taro from "@tarojs/taro";
import React, { ReactNode } from "react";

interface ISocketMessageProps {
  room_id: string;
  region: {
    latitude: string;
    longitude: string;
  };
}
export const socketMessage = (props, ref) => {
  const { room_id = "", region = {} } = props;
  return new Promise((resolve, reject) => {
    ref = Taro.connectSocket({
      url: "wss://www.kocoffee.top/ws/",
    }).then((task) => {
      task.onOpen(function () {
        console.log("onOpen");
        const joinRoom = JSON.stringify({
          path: "JoinRoom",
          room_id,
          local: region,
        });

        task.send({ data: joinRoom });
        task.onMessage((msg) => {
          console.log(msg, "接受消息");
          const newMsg = JSON.parse(msg?.data);
          resolve(newMsg);
        });
      });
    });
  });
};


interface IMegObjParams {
  RoomMsgPush?: any
  RoomUserPush?: any
  LoginRsp?: any
}

export function msgAction(msg) {
  let megObj: IMegObjParams = {};
  if (msg.path === "RoomMsgPush") {
    megObj = {
      RoomMsgPush: {
        ...msg.rsp,
        left: true
      },
    };
  }
  if (msg.path === "RoomUserPush") {
    megObj = {
      ...megObj,
      RoomUserPush: msg.rsp,
    };
  }
  if(msg.path === "LoginRsp"){
    megObj = {
      ...megObj,
      LoginRsp: msg.rsp,
    };
  }
  return megObj;
}
