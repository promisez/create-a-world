import Taro from "@tarojs/taro";

export default class ChatRoomRecord {
  private recoedManage: Taro.RecorderManager | null;
  private timer: NodeJS.Timeout | null;
  constructor() {
    this.timer = null;
    this.recoedManage = null;
    this.init = this.init.bind(this);
    this.stop = this.stop.bind(this);
    this.start = this.start.bind(this);
  }
  init() {
    this.timer && clearTimeout(this.timer);
    this.stop();
    this.recoedManage = Taro.getRecorderManager();
    this.start(this.recoedManage);
    // 监听开始事件
    this.recoedManage.onStart(() => {
      console.log("开始录音");
    });
    // 监听停止事件
    this.recoedManage.onStop((res) => {
      const { tempFilePath } = res;
      // 返回的base64格式的
      console.log("结束录音", res, res.tempFilePath);
    });
    // 监听已录制完指定帧大小的文件事
    this.recoedManage.onFrameRecorded((res) => {
      const { frameBuffer } = res;
      console.log("frameBuffer.byteLength", frameBuffer.byteLength);
    });
    // 以下场景会触发此事件：微信语音聊天、微信视频聊天。此事件触发后，录音会被暂停,这里直接结束录音
    this.recoedManage.onInterruptionBegin((res) => {
      console.log(res, "特殊中断录音");

      this.stop();
    });
    this.timer = setTimeout(function () {
      //结束录音
      // Taro.stopRecord();
      this.stop();
    }, 10000);
  }
  stop() {
    this.recoedManage?.stop();
  }
  start(instance) {
    instance.start({
      duration: 60000, // 录音最大时长 60s
      sampleRate: 44100, // 采样率 44.1kHz 采样率越大声音越自然
      numberOfChannels: 1, // 录音通道数
      encodeBitRate: 192000, // 编码码率，有效值见下表格 相当于是声音转成数据
      format: "mp3", // 录音保存的格式
      frameSize: 50, // 指定帧大小，单位 KB。传入 frameSize 后，每录制指定帧大小的内容后，会回调录制的文件内容，不指定则不会回调。暂仅支持 mp3 格式。
    });
  }
}

/**
 * numberOfChannels : 声道数是指音频数据记录和播放时的通道数量。单声道是指声音由一个通道录制而成，所以播放时只需要一个扬声器。双声道，又称为立体声，是指声音由两个通道录制而成，通常一个通道记录左声道的声音，另一个通道记录右声道的声音。播放时需要两个扬声器，左声道的声音播放在左边的扬声器，右声道的声音播放在右边的扬声器。
 */
