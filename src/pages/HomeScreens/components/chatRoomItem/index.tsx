import { Button, Cell, SwipeCell } from "@taroify/core";
import { View } from "@tarojs/components";
import React from "react";
import { Arrow } from "@taroify/icons";
import "./index.less";

interface IChatRoomItemProps {
  content: string;
  isShowDelete?: boolean;
  toFn?: () => void;
  deleteFn?: () => void;
}

function ChatRoomItem(props: IChatRoomItemProps) {
  const { content, isShowDelete = false, deleteFn = () => {}, toFn = () => {} } = props;
  return (
    <View className="ChatRoomItemBox" onClick={toFn}>
      <SwipeCell className="custom-swipe-cell">
        <Cell bordered={false} title={content}>
          <Arrow color="#B4B4B4" size={14} />
        </Cell>
        {isShowDelete && (
          <SwipeCell.Actions side="right">
            <Button variant="contained" shape="square" color="danger" onClick={deleteFn}>
              删除
            </Button>
          </SwipeCell.Actions>
        )}
      </SwipeCell>
    </View>
  );
}

export default ChatRoomItem;
