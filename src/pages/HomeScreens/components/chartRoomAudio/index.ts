import Taro from "@tarojs/taro";

export default class ChatRoomAudio {
  private context: Taro.InnerAudioContext | null;
  constructor() {
    this.context = null;
    this.init = this.init.bind(this);
    this.stop = this.stop.bind(this);
  }

  init(url: string) {
    this.stop()
    this.context = Taro.createInnerAudioContext();
    this.context.autoplay = true;
    this.context.src = url;
    this.context.onPlay(() => {
      console.log("开始播放");
    });
    this.context.onStop(() => {
      console.log('播放手动结束');
    });
    this.context.onEnded(() => {
      console.log("播放结束完毕了");
    });
    this.context.onError((res) => {
      console.log(res.errMsg, res.errCode, "播放失败");
    });
  }

  stop() {
    this.context?.stop();
  }
}
