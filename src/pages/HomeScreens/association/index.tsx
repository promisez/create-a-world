import { getLoad, setLoad, shareIntercept } from "@/common/utils";
import Taro, { useShareAppMessage } from "@tarojs/taro";
import React, { useEffect, useState } from "react";
import Header from "@/components/common-header";
import NavTopBar from "@/components/nav-bar";
import { View, Image, PageContainer, ScrollView } from "@tarojs/components";
import PopUpHeader from "@/components/popup-header";
import icons from "@/common/icon";
import { getDeviceList } from "@/api/order";
import CommonLocationSelect from "@/components/common-location-select";
import { LocationList } from "@/atypes/order";
import "./index.less";

function Association() {
  useShareAppMessage(shareIntercept);
  const [showSelecttMachine, setShowSelectMachine] = useState(false);
  const [regionList, setRegionList] = useState<LocationList[]>([]);
  const [currentLocation, setCurrentLocation] = useState<LocationList>();

  const getDeviceLs = async ({ latitude, longitude }) => {
    const data = await getDeviceList({ longitude, latitude });
    // const online = data?.filter((v)=> v.online !== 'OFFLINE')
    // console.log(online,'onlineonlineonlineonlineonline');
    
    setRegionList(data);
    setLoad("region", data?.[0] ?? {});
    setCurrentLocation(data[0] ?? {});
  };
  useEffect(() => {
    Taro.getLocation({
      type: "wgs84",
      success: function (res) {
        const { latitude, longitude } = res;
        getDeviceLs({ latitude, longitude });
        setLoad("currentRegion", { latitude, longitude });
      },
      fail: function () {
        Taro.showToast({ title: "获取位置失败", duration: 1000, icon: "none" });
      },
    });
    if (getLoad("currentRegion")) {
      getDeviceLs(getLoad("currentRegion"));
    }
  }, []);
  return (
    <View className="associationPage">
      <NavTopBar />
      <Header title="咖友社群" />
      <ScrollView scrollY className="associationScroll">
        <View className="associationContent">
          <Image className="bg" src={icons.img_kayou_bg} />
          <View className="QRContainer">
            <View className="chooseMachine" onClick={() => setShowSelectMachine(true)}>
              <View className="leftLocation">
                <Image src={icons.icon_location} className="icon" />
                {!!currentLocation ? (
                  <View className="active_tag">{currentLocation?.name}</View>
                ) : (
                  <View className="text">请选择咖啡机</View>
                )}
              </View>
              <Image src={icons.icon_btn_down1} className="icon" />
            </View>
            <View className="noMachine"></View>
            {currentLocation?.friend_group?.length ? (
              <Image className="img" src={currentLocation?.friend_group ?? ""} />
            ) : (
              <View className="imgerr">加载二维码失败了</View>
            )}

            <View className="tip">长按保存二维码</View>
          </View>
        </View>
      </ScrollView>
      <PageContainer
        show={showSelecttMachine}
        className="tag_container"
        position="bottom"
        round
        onAfterLeave={() => setShowSelectMachine(false)}
      >
        <View className="header_btn">
          <PopUpHeader onClick={() => setShowSelectMachine(false)} />
        </View>
        <ScrollView
          className="scroll_container"
          scrollY
          scrollWithAnimation
          scrollTop={0}
          style={{ height: "304px" }}
        >
          <CommonLocationSelect
            localtionArr={regionList}
            currentId={currentLocation?.device_id}
            changeLocationFn={(it) => setCurrentLocation(it)}
          />
        </ScrollView>
      </PageContainer>
    </View>
  );
}

export default Association;
