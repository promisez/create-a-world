import { View } from "@tarojs/components";
import NavTopBar from "@/components/nav-bar";
import CommonHeader from "@/components/common-header";
import "./index.less";
import Taro from "@tarojs/taro";
import ChatRoomItem from "../components/chatRoomItem";
import { deleteCharRoomItem, getCharRoomList } from "@/api/charRoom";
import { useEffect, useState } from "react";
import { getLoad } from "@/common/utils";
import { LocationList } from "@/atypes/order";

function ChatRoomList() {
  const [chatRoomList, setChatRoomList] = useState([]);
  const [currentRegion, setCurrentRegion] = useState<LocationList>();

  function getChatRoomListFn() {
    getCharRoomList().then((res) => {
      console.log(res, "获取房间列表");
      setChatRoomList(res ?? []);
    });
  }

  function deleteChatRoomFn(id) {
    deleteCharRoomItem(id).then((res) => {
      console.log(res, "删除房间");
      getChatRoomListFn();
    });
  }

  const getDeviceLs = () => {
    const data = getLoad("region_list");
    // const online = data?.filter((item) => item.online !== "OFFLINE");
    console.log(data, "机器列表");
    setCurrentRegion(data?.[0] ?? {});
  };

  useEffect(() => {
    getChatRoomListFn();
    getDeviceLs();
  }, []);
  const onEnter = () => {
    Taro.navigateTo({
      url: `/pages/HomeScreens/chatroom/index?roomId=${currentRegion?.device_id}`,
    });
  };
  return (
    <View className="chatRoomListContainer">
      <NavTopBar />
      <CommonHeader title="聊天室列表" />
      <View className="content">
        <View className="title">当前可进入</View>
        {currentRegion?.name?.length ? (
          <ChatRoomItem content={currentRegion?.name} toFn={onEnter} />
        ) : <View className="nomore">没有可进入的房间</View>}
        <View className="title">过往记录</View>
        {chatRoomList?.map((_, i) => (
          <ChatRoomItem
            content="深大粤海校区"
            isShowDelete
            key={i}
            deleteFn={() => deleteChatRoomFn(_)}
          />
        ))}
        <View className="nomore">没有更多了</View>
      </View>
    </View>
  );
}

export default ChatRoomList;
