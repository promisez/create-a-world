import { View } from "@tarojs/components";
import React from "react";
import ChatRoomAudio from "../components/chartRoomAudio";
import ChatRoomRecord from "../components/chatRoomRecord";

const url1 = "https://storage.360buyimg.com/jdrd-blog/27.mp3";
const url2 = "https://zb042901.aoss.cn-sh-01.sensecoreapi-oss.tech/lrfc.mp3";

function AudioComponentTest() {
  const instance = new ChatRoomAudio();
  const recordInstance = new ChatRoomRecord();
  return (
    <View>
      <View onClick={() => instance.init(url1)}>播放music1</View>
      <View onClick={() => instance.stop()}>停止播放</View>
      <View onClick={() => instance.init(url2)}>播放music2</View>
      <View onClick={() => instance.stop()}>停止播放</View>
      <View
        onClick={() => {
          recordInstance.init();
        }}
      >
        点击开始录音
      </View>
      <View
        onClick={() => {
          recordInstance.stop();
        }}
      >
        点击暂停录音
      </View>
      F
    </View>
  );
}

export default AudioComponentTest;
