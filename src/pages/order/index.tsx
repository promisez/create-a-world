import NavTopBar from "@/components/nav-bar";
import { Location } from "@taroify/icons";
import { Image, PageContainer, Text, View } from "@tarojs/components";
import Taro, {
  Events,
  useDidShow,
  useRouter,
  useShareAppMessage,
  useTabItemTap,
} from "@tarojs/taro";
import { useEffect, useState } from "react";

import { getDeviceList } from "@/api/order";
import { LocationList } from "@/atypes";
import PopUpHeader from "@/components/popup-header";
import { Backdrop, Popup } from "@taroify/core";
// import OrderRegion from "./components/order-step-title";
import "./index.less";
import { cardList } from "./index.preset";
import { distanceConverFn, getLoad, setLoad, shareIntercept } from "@/common/utils";
import OrderRegion from "../OrderScreens/components/order-step-title";
import { toast } from "@/api/util";

export default function Order() {
  const [showRegionPop, setShowRegionPop] = useState(false);
  const [regionIndex, setRegionIndex] = useState(0);
  const [regionInfo, setRegionInfo] = useState<LocationList>();
  const [regionList, setRegionList] = useState<LocationList[]>([]);
  useShareAppMessage(shareIntercept);
  useDidShow(() => {
    setShowRegionPop(true);
  });

  const getDeviceLs = () => {
    const data = getLoad("region_list");
    setRegionList(data);
    setShowRegionPop(true);
    setLoad("region", data[0]);
    setRegionInfo(data[0]);
  };
  useEffect(() => {
    getDeviceLs();
    setLoad("shopCar", JSON.stringify({ coffee: [], cup: [], image: [], isChecked: false }));
  }, []);
  const selectRegion = (e, index) => {
    e.stopPropagation();
    setRegionInfo(regionList[index]);
    setLoad("region", regionList[index]);
    setRegionIndex(index);
  };
  const onShowRegionPopup = () => {
    setShowRegionPop(true);
  };

  const cardClick = (item) => {
    Taro.navigateTo({
      url: item.url,
    });
  };

  return (
    <View className="orderContainer">
      <NavTopBar />
      <OrderRegion {...regionInfo} onClick={onShowRegionPopup} />
      <View className="cardList">
        {cardList.map((item, index) => {
          return (
            <View onClick={() => cardClick(item)} className="item" key={index}>
              <Image className="orderImg" src={item.src}></Image>
            </View>
          );
        })}
      </View>

      <PageContainer
        show={showRegionPop}
        onClickOverlay={() => setShowRegionPop(false)}
        round
        className="backdrop"
      >
        <View className="backdropBox">
          <PopUpHeader onClick={() => setShowRegionPop(false)} />
          <View className="regionListBox">
            {regionList.map((item, index) => {
              return (
                <View
                  className={`item ${regionIndex === index ? "item-active" : ""}`}
                  key={index}
                  onClick={(e) => {
                    selectRegion(e, index);
                    setTimeout(() => {
                      setShowRegionPop(false);
                    }, 300);
                  }}
                >
                  <View className="itemLeft">
                    <View className="regionIcon">
                      <Location color="red" size={14} />
                    </View>
                    <View className="regionContent">
                      <View className="regionInfo">
                        <Text className="regionName">{item.address}</Text>
                        <Text className="regionTip"></Text>
                      </View>
                      <Text className="regionAddress">{item.name}</Text>
                    </View>
                  </View>
                  {regionIndex === index && (
                    <View className="itemRight">
                      <Text className="currentSelect">当前选择</Text>
                    </View>
                  )}
                </View>
              );
            })}
          </View>
        </View>
      </PageContainer>
    </View>
  );
}
