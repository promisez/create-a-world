import { View, Image, Text, ScrollView } from "@tarojs/components";
import React, { useEffect, useState } from "react";
import NavTopBar from "@/components/nav-bar";
import Headers from "@/components/common-header";
import icons from "@/common/icon";
import { getCoffeeDetail } from "@/api/order-detail";
import CommonOrderNum from "@/components/common-order-num";
import { shareIntercept, showTimeFn } from "@/common/utils";
import { getCurrentInstance } from "@tarojs/runtime";
import { QRCode } from "taro-code";
import Taro, { useShareAppMessage } from "@tarojs/taro";
import { orderListRes } from "@/api/my-order-list";
import ShowOrderTime from "./components/show-order-time";
import "./index.less";
import ProductItem from "./components/product-item";
import { stateMap } from "./index.preset";
import OrderRegion from "../components/order-step-title";

function OrderDetail() {
  useShareAppMessage(shareIntercept);
  const [currentOrder, setCurrentOrder] = useState<orderListRes>();
  const [loading, setLoading] = useState(false);
  const [waitNum, setWaitNum] = useState(0);

  function getOrderDetailFn(id) {
    getCoffeeDetail(id).then((res) => {
      console.log(res, "获取咖啡制作详情");
      setCurrentOrder(res);
      setLoading(false);
      // 前面还有几杯 (FinishTime - 当前时间) / 90  【90s是完成一杯的时间】
      const currentTime = new Date().getTime() / 1000;
      const num = ((res?.FinishTime ?? 0) - currentTime) / 90;
      setWaitNum(num > 0 ? Math.floor(num) : 0);
    });
  }

  function showProductNumFn(PayInfo) {
    const { List, card_add } = PayInfo ?? {};
    let num = List?.length ?? 0;
    if (card_add === 10) {
      num += 1;
    }
    return num;
  }

  useEffect(() => {
    const { router } = getCurrentInstance();
    let timer = setInterval(() => {
      getOrderDetailFn(router?.params?.order_id ?? "没有order_id");
    }, 4000);
    return () => {
      clearTimeout(timer);
    };
  }, []);

  useEffect(() => {
    const { router } = getCurrentInstance();

    getOrderDetailFn(router?.params?.order_id ?? "没有order_id");
  }, []);

  const refreshStatus = () => {
    setLoading(true);
    getOrderDetailFn(currentOrder?.TakeInfo?.order_id ?? "");
    Taro.showToast({
      title: "刷新成功",
    });
  };

  const coffeeWindow = currentOrder?.MakeState?.Coffees?.map((k) => k.take_window) ?? []

  return (
    <View className="order-detail-container">
      <NavTopBar />
      <Headers title="订单详情" isShowBack={false} />
      <ScrollView
        scrollY
        className="orderInfo"
        scrollWithAnimation
        refresherEnabled
        refresherTriggered={loading}
        onRefresherRefresh={refreshStatus}
      >
        <View className="take-meal-status">
          <Image src={icons.icon_order_logo} className="logo-img" />
          <View className="title">{typeof currentOrder?.OrderTakeNum === 'number' ? `A${currentOrder?.OrderTakeNum}` : '排队中'}</View>

          <QRCode
            className="qrcode-img"
            text={currentOrder?.TakeInfo?.qr_param ?? ""}
            size={215}
            scale={4}
            errorCorrectLevel="M"
            typeNumber={2}
          />
          {(currentOrder?.DisplayState ?? 0) === 0 && (
            <View className="tip">
              前面还有{waitNum}杯,预计
              <Text className="time">
                {showTimeFn((currentOrder?.FinishTime ?? 0) * 1000, false)}
              </Text>
              可制作完成
            </View>
          )}
          {(currentOrder?.DisplayState ?? 0) === 1 && (
            <View className="tip">
              正在制作,预计
              <Text className="time">
                {showTimeFn((currentOrder?.FinishTime ?? 0) * 1000, false)}
              </Text>
              可制作完成
            </View>
          )}
          {(currentOrder?.DisplayState ?? 0) === 2 && (
            <View className="tip">
              制作完成 ，饮品已放在 <Text className="time">{`${coffeeWindow} 号窗口`}</Text>
            </View>
          )}
          {(currentOrder?.DisplayState ?? 0) === 3 && (
            <View className="tip">{`饮品已从 ${coffeeWindow} 号窗口取走`}</View>
          )}
        </View>
        <View className="address">
          <OrderRegion device_id={currentOrder?.MakeInfo?.device_id ?? ""} />
        </View>

        <View className="order-list-show">
          <CommonOrderNum orderNumber={currentOrder?.TakeInfo?.order_id} />
          {currentOrder?.MakeState?.Coffees?.map((item, index) => {
            return <ProductItem productInfo={item} key={index} />;
          })}
          <View className="lessContent">
            <View className="lessTitle">创界优惠</View>
            <View className="price">-￥{currentOrder?.PayInfo?.less_price}</View>
          </View>
          <View className="totalContent">
            <View className="num">{`共计${showProductNumFn(currentOrder?.PayInfo)}件商品`}</View>
            <View className="price">
              <Text className="title">实付 </Text>
              <Text className="totalPrice">￥{currentOrder?.PayInfo?.final_price}</Text>
            </View>
          </View>
        </View>
        <ShowOrderTime time={showTimeFn((currentOrder?.OrderTime ?? 0) * 1000)} />
        <View className="bottom-scroll" />
      </ScrollView>
      <View className="bottomButton">
        <View
          className="text"
          onClick={() =>
            Taro.switchTab({
              url: "/pages/home/index",
            })
          }
        >
          返回首页
        </View>
      </View>
    </View>
  );
}

export default OrderDetail;
