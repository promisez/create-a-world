import { View, Text } from "@tarojs/components";
import React from "react";
import "./index.less";

function ShowOrderTime(props) {
  const { time } = props;
  return (
    <View className="orderTimeContainer">
      <Text className="text">下单时间: {time}</Text>
    </View>
  );
}

export default ShowOrderTime;
