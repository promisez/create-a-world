import React from "react";
import { View, Image } from "@tarojs/components";
import "./index.less";

function ProductItem(props) {
  const { productInfo } = props;
  console.log(productInfo,'productInfo==productInfo');
  
  const { coffee, make_info } = productInfo;
  return (
    <View className="orderDetailProductItemComtainer">
      <View className="productItem">
        <Image src={make_info?.coffee?.pic ?? ''} className="itemImg" />
        <View className="itemInfo">
          <View className="name">{make_info?.coffee?.name ?? ''}</View>
          <View className="chooseInfo">{`${make_info?.cool}+${make_info?.sweet}`}</View>
        </View>
        <View className="itemPrice">
          <View className="price">￥ {make_info?.coffee?.price ?? 0}</View>
          <View className="num">x 1</View>
        </View>
      </View>
    </View>
  );
}

export default ProductItem;
