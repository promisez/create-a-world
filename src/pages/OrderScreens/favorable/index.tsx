import { Checkbox, FixedView } from "@taroify/core";
import { Image, View, Text, ScrollView } from "@tarojs/components";
import Taro, { useRouter } from "@tarojs/taro";
import { useEffect, useMemo, useState } from "react";
import Header from "../../../components/common-header";
import NavTopBar from "../../../components/nav-bar";
import "./index.less";
import icons from "@/common/icon";
import { getCouponCard, getCouponList } from "@/api/coffee";
import { toast } from "@/api/util";
import { getLoad, setLoad } from "@/common/utils";

function Favorable() {
  // 正在使用的卡或券
  const initPayload = getLoad("shop-payload");

  const payload = useMemo(() => (initPayload ? initPayload : initPayload), [initPayload]);
  const { params } = useRouter();
  // 要修改的那一杯
  const { index = "", onlyWatch, sys_id: initSysId } = params;

  const payCouponId = payload?.list?.[index]?.coupon_id;

  const [list, setList] = useState<any[]>([]);
  const [actIndex, setActIndex] = useState();
  const [cardInfo, setCardInfo] = useState<Record<string, any>>({});

  const onSelect = (type, index, sys_id) => {
    if (actIndex === index) return setActIndex(undefined);
    if (type === "coupon") {
      /**
       * 避开以下条件，则可以成功切换优惠券
       * 1、该券是否同系列
       * 2、该券是否正在使用
       * 3、该券是否和所选咖啡使用的券id相同
       */
      if (sys_id !== initSysId) {
        toast("优惠券系列不同");
        return;
      }
      const findIndex = payload?.list?.findIndex((v) => v.coupon_id === list[index]?.coupon_id);
      const isRepeat = findIndex > -1;
      if (isRepeat && payCouponId !== payload?.list?.[findIndex]?.coupon_id) {
        toast("此优惠券正被其他商品使用中");
        return;
      }
      setActIndex(index);
    }
    if (type == "card") {
      if (+cardInfo?.num <= 0) {
        toast("优惠卡次数不足");
        return;
      }
      setActIndex(index);
    }
  };

  const onNext = () => {
    // 结合卡和券的信息,修改完shop-payload再下一步
    if (!actIndex) {
      // 取消优惠
      payload.list[index].coupon_id = "";
    }
    if (actIndex === -1) {
      // 选的是卡
      payload.list[index].coupon_id = cardInfo?.sys_id;
      console.log(payload, "payload");
    }
    if (actIndex && actIndex > -1) {
      // 选的是券
      payload.list[index].coupon_id = list?.[actIndex]?.coupon_id ?? "";
    }
    setLoad("shop-payload", payload);
    Taro.navigateBack();
  };
  const getData = async () => {
    // 请求次卡信息
    const res = await getCouponCard();
    // 优惠券列表
    const list = await getCouponList();
    const defaultIndex = (() => {
      if (payCouponId === "0") return -1;
      if (payCouponId) {
        return list.findIndex((item) => item.id === payCouponId);
      }
      return;
    })();
    setActIndex(defaultIndex);
    const newList = list.map((item, index) => {
      return {
        time: item?.expire_time,
        name: item?.name,
        coupon_id: item?.id,
        value: item?.value,
        discount: item?.discount,
        sys_id: item?.sys_id,
      };
    });
    setList(newList);
    const { buy_num, use_num } = res;
    res.info.num = +buy_num - +use_num;
    setCardInfo(res?.info);
  };
  useEffect(() => {
    getData();
  }, []);

  const Item = ({ title, tip, type, index, discount, sys_id, time, value }) => (
    <View
      className={`favorableItem ${actIndex === index ? "active" : ""}`}
      onClick={() => !onlyWatch && onSelect(type, index, sys_id)}
    >
      <View className="favorableLeft">
        <Image className="favorableImg" src={index === -1 ? icons.youhuika : icons.youhuiquan} />
        {!value ? discount && (
          <View className="favorableLess">
            <Text className="favorableNum">{discount}</Text>
            <Text className="favorableTxt">折</Text>
          </View>
        ) : <View className="favorableLess">
            <Text className="favorableNum">超值</Text>
          </View>}
        <View className="favorableInfo">
          {!value ? <View className="favorableName">{title}</View> : <View className="favorableName">{tip}</View>}
          {!value ? <View className="favorableTip">{tip}</View> : <View className="favorableTip">所有商品仅需￥{value}</View>}
          <View className="favorableTip">有效期：{time}</View>
        </View>
      </View>
      {onlyWatch && (
        <View className="favorableBtnBox">
          <View
            className="favorableBtn"
            onClick={() => Taro.switchTab({ url: `/pages/coffee/index` })}
          >
            立即使用
          </View>
        </View>
      )}
    </View>
  );

  function toDate(timestamp) {
    const date = new Date(timestamp * 1000);

    const year = date.getFullYear();
    const month = ("0" + (date.getMonth() + 1)).slice(-2);
    const day = ("0" + date.getDate()).slice(-2);

    const dateString = year + "/" + month + "/" + day;
    return dateString;
  }

  const cardTime = toDate(cardInfo?.expire_time);

  return (
    <View className="favorableContainer">
      <NavTopBar />
      <Header title={onlyWatch ? "优惠券" : "修改优惠"} />
      <ScrollView
        scrollY
        scrollWithAnimation
        className="favorableList"
        refresherEnabled
        refresherTriggered={false}
        onRefresherRefresh={() => { }}
        onScrollToLower={() => { }}
      >
        {!onlyWatch && (
          <Item
            time={cardTime}
            discount=""
            title="优惠卡"
            tip={`还有${cardInfo?.num}次`}
            key="card"
            type="card"
            index={-1}
            sys_id={"0"}
            value={0}
          />
        )}
        {list?.map((item, index) => {
          return (
            <Item
              time={toDate(item?.time)}
              discount={item?.discount}
              title="优惠券"
              tip={item?.name}
              index={index}
              key={index}
              type="coupon"
              sys_id={item?.sys_id}
              value={item.value}
            />
          );
        })}
        {!onlyWatch && (
          <FixedView className="favorableFixed" position="bottom">
            <View className="favorableBtn" onClick={onNext}>
              确定
            </View>
          </FixedView>
        )}
        <View style={{ height: "100px" }}></View>
      </ScrollView>
    </View>
  );
}

export default Favorable;
