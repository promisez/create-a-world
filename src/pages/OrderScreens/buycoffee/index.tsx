import Header from "@/components/common-header";
import NavTopBar from "@/components/nav-bar";
import { Text, View } from "@tarojs/components";
import Taro, { useDidShow, useRouter, useShareAppMessage } from "@tarojs/taro";
import React, { useEffect, useMemo, useRef, useState } from "react";
import BuyBottomBtn from "../components/buy-bottom-btn";
import CountCard from "../components/count-card";
import OrderItem from "../components/order-item";
import OrderRegion from "../components/order-step-title";
import "./index.less";
import defaultData from "./index.preset";
import { calculatePrice, getCouponCard, pay } from "@/api/coffee";
import { getLoad, setLoad, shareIntercept } from "@/common/utils";
import { toast } from "@/api/util";
import { debounce } from "../../coffee/index.preset";

function BuyCoffee() {
  const [list, setList] = useState(defaultData);
  // 会从coffee页面来/会从“修改优惠”页面来
  const payload = useMemo(() => getLoad("shop-payload"), []);
  const [total, setTotal] = useState("");
  const [lessTotal, setLessTotal] = useState("");
  const [isChecked, setIsChecked] = useState(payload.isChecked);
  const [showBuyCard, setShowBuyCard] = useState(true);
  const { params } = useRouter();
  const spendRef = useRef(false);
  useShareAppMessage(shareIntercept);
  const options = useMemo(() => {
    // 根据上个页面的选择，生成对应的配置
    const shopData = getLoad("shopCar");
    return JSON.parse(shopData)?.coffee?.map((item) => ({
      model_id: item.model_id,
      title: item.name,
      detail: `${item.sweet}+${item.ice}`,
      sweet: item.sweet,
      ice: item.ice,
    }));
  }, []);

  const getList = async () => {
    const initPayload = getLoad("shop-payload");
    const res = await calculatePrice(initPayload);
    // 根据less_type生成新的payload
    const newlist = res?.List?.map((item, index) => {
      const coupon_id = ["", "0", item?.coupon?.id][item.less_type];
      return {
        ...initPayload?.list?.[index],
        coupon_id,
      };
    });
    const newPayLoad = {
      ...initPayload,
      list: newlist,
    };
    // 存储给“修改优惠”页面使用、“修改优惠”页面也会返回新的payload
    setLoad("shop-payload", newPayLoad);
    // 生成可渲染的list
    const list = res?.List?.map((v) => v.coffee)?.map?.((item, index) => {
      // coffee层的id
      const oItem = options.find((o) => o.model_id === item.model_id);
      // 优惠券id
      const coupon_id = newPayLoad.list[index].coupon_id;
      // 最外层的id
      const coffee_model_id = res?.List[index]?.coffee_model_id;
      // 系列id
      const sys_id = res?.List[index]?.coupon?.sys_id;

      return {
        ...oItem,
        coffee_model_id,
        sys_id,
        coupon_id,
        url: item.pic,
        price: item.price,
        hand_price: item.hand_price,
        less_price: item.less_price,
        less_type: res?.List?.[index]?.less_type,
        // less_type: 1,
      };
    });
    setList(list);
    setTotal(res?.final_price);
    setLessTotal(res?.less_price);
  };
  useDidShow(() => {
    setLoad('isEnterBuyCoffee',true)
    getList();
    getCard();
  });
  const getCard = async () => {
    const res = await getCouponCard();
    const { buy_num, use_num } = res;
    setShowBuyCard(Boolean(+buy_num - +use_num));
  };
  const onChange = (index: number, sys_id: string) => {
    Taro.navigateTo({
      url: "/pages/OrderScreens/favorable/index?index=" + index + "&sys_id=" + sys_id,
    });
  };
  const onSelectChange = () => {
    const initpayload = getLoad("shop-payload");
    setIsChecked(!isChecked);
    initpayload.isChecked = !isChecked;
    setLoad("shop-payload", initpayload);
    getList();
  };
  const onSpend = async () => {
    if (spendRef.current) return;
    Taro.showLoading({ title: "支付中" });
    spendRef.current = true;
    const use_card = payload.isChecked;
    const coffees = list.map((item) => ({
      sweet: item.sweet,
      cool: item.ice,
      coupon_id: item.coupon_id,
      coffee_model_id: item.coffee_model_id,
      less_type: item.less_type,
    }));
    const device_id = getLoad("region").device_id;

    const res = await pay({ use_card, coffees, device_id }).catch((err) => {
      spendRef.current = false;
    });
    Taro.hideLoading();
    if (!res) return;

    Taro.requestOrderPayment({
      ...res,
      success: () => {
        Taro.navigateTo({
          url: `/pages/OrderScreens/orderdetail/index?order_id=${res?.OrderId ?? ""}`,
        });
        setLoad('isEnterBuyCoffee',false)
        setLoad("shopCar", JSON.stringify({ coffee: [], cup: [], image: [], isChecked: false }));
      },
      fail: (err) => {
        toast("支付失败 " + total);
        console.log(err, "requestOrderPayment err");
      },
      complete: () => {
        spendRef.current = false;
      },
    });
  };

  return (
    <View className="buyCoffeeContainer">
      <NavTopBar />
      <Header title="确认订单" />
      <View className="coffeeOrderList">
        <View className="buyCoffeeRegion">
          <OrderRegion device_id={params?.device_id} />
        </View>
        <Text className="listTotal">共计{list?.length}件商品</Text>

        {list?.map((item, index) => {
          return <OrderItem onChange={onChange} key={index} item={item} index={index} />;
        })}
        {showBuyCard && <CountCard showSelect onChange={onSelectChange} checked={isChecked} />}
        <View style={{ height: 48 }} />
      </View>
      <BuyBottomBtn onSpend={debounce(onSpend, 500)} price={total} favorable={lessTotal} />
    </View>
  );
}

export default BuyCoffee;
