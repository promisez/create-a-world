import { Tag } from "@taroify/core";
import { View } from "@tarojs/components";
import React from "react";
import './index.less'

function ChooseCoffeeTag(props) {
  const { text, current } = props;
  return (
    <View className="chooseCoffeeTagContainer">
      <Tag className={current ? "coffeeTag coffeeTagActive" : 'coffeeTag'}>{text}</Tag>
    </View>
  );
}

export default ChooseCoffeeTag;
