import React from "react";
import { View, Image } from "@tarojs/components";
import { Checkbox } from "@taroify/core";
import icons from "@/common/icon";
import "./index.less";
import Taro from "@tarojs/taro";

function CarDiscountCard() {
  return (
    <View className="car-discount-card-container">
      <Image src={icons.icon_buy_card} className="img" />
      <View className="center">
        <View className="title">购买次卡</View>
        <View className="describe">购买后即可享受7次优惠</View>
      </View>
      <View
        className="discount-rule"
        onClick={(e) => {
          e.stopPropagation()
          Taro.navigateTo({
            url: "/pages/MeScreens/cardRule/index",
          });
        }}
      >{`优惠规则>`}</View>
    </View>
  );
}

export default CarDiscountCard;
