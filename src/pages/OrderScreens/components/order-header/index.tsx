import { View, Text } from "@tarojs/components";
import React from "react";
import { ArrowLeft } from "@taroify/icons";
import Taro from "@tarojs/taro";
import "./index.less";

function OrderStepHeader(props) {
  const { step, title = '选杯子' } = props;
  return (
    <View className="OrderStepHeader">
      <ArrowLeft size={18} onClick={() => Taro.navigateBack()} color="#000" />
      <View className="step">
        <Text style={{ color: "#E6051F" }}>{step}</Text>
        {/* <Text>{`/3 ${title}`}</Text> */}
        <Text>{`/1 ${title}`}</Text>
      </View>
    </View>
  );
}

export default OrderStepHeader;
