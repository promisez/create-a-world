import icons from "@/common/icon";
import { View, Image } from "@tarojs/components";
import React from "react";
import { Checkbox } from "@taroify/core";
import "./index.less";
import Taro from "@tarojs/taro";

interface IChooseDiscountCard {
  isChecked: boolean;
  changeFn: (val: boolean) => void;
}
function ChooseDiscountCard(props: IChooseDiscountCard) {
  const { isChecked, changeFn } = props;
  return (
    <View className="choose-discount-card-container">
      <View className="center">
        <View className="title-content ">
          <Image src={icons.icon_buy_card} className="img" />
          <View className="title">购买次卡</View>
          <View
            className="discount-rule"
            onClick={(e) => {
          e.stopPropagation()

              Taro.navigateTo({
                url: "/pages/MeScreens/cardRule/index",
              });
            }}
          >{`优惠规则>`}</View>
        </View>
        <View className="describe">购买后即可享受7次优惠</View>
      </View>
      <Checkbox
        className="cardCheckbox"
        size={16}
        shape="square"
        checked={isChecked}
        onChange={(val) => changeFn(val)}
      />
    </View>
  );
}

export default ChooseDiscountCard;
