import icons from "@/common/icon";
import { Badge } from "@taroify/core";
import { View, Image } from "@tarojs/components";
import { calculatePrice, calculatePriceRes } from "@/api/coffee";
import React, { useEffect } from "react";
import "./index.less";

interface IShowShopCar {
  onShowCar: () => void;
  carNum: number;
  priceData: calculatePriceRes;
  toSettlementFn: () => void;
  checkedCard: boolean;
}

function ShowShopCar(props: IShowShopCar) {
  const { onShowCar, carNum, priceData, toSettlementFn, checkedCard } = props;

  const isToBuy = (carNum > 0 && !checkedCard) || (checkedCard && carNum > 1);

  return (
    <View className="car-show-choose-click">
      {/* 没有添加商品到购物车就不展示 */}

      {!!carNum ? (
        <Badge content={carNum}>
          <Image
            src={icons.icon_gwd}
            className="shop-car-img"
            onClick={(e) => {
              e.stopPropagation();
              onShowCar();
            }}
          />
        </Badge>
      ) : (
        <View className="img-box">
          <Image
            src={icons.icon_gwd}
            className="shop-car-img"
            onClick={(e) => {
              e.stopPropagation();
              onShowCar();
            }}
          />
        </View>
      )}
      {isToBuy && (
        <View className="show-choose-info">
          <View className="discount-money">
            <View className="expected">预计</View>
            <View className="money">￥{priceData?.final_price ?? 0}</View>
            {/* <View className="original-price">
              原价¥
              {carShopData?.coffee?.reduce((a, b) => a + Number(b.price) * b.currentNum, 0)}
            </View> */}
          </View>
          <View className="discount-detail">{`使用优惠卡,共计减免¥${
            priceData?.less_price ?? 0
          }`}</View>
        </View>
      )}
      <View
        className={isToBuy ? "to-settlement" : "to-settlement disabled-settlement"}
        onClick={() => {
          if (isToBuy) {
            toSettlementFn();
          }
        }}
      >
        去结算
      </View>
    </View>
  );
}

export default ShowShopCar;
