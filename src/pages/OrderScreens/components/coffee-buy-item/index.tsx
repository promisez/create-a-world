import { Image, View, Text } from "@tarojs/components";
import React from "react";
import { Plus } from "@taroify/icons";
import "./index.less";

export default function CoffeeBuyItem(props) {
  const { data, onChooseCoffee } = props;
  const { pic, name, price, hand_price, mc_price, decs = '' } = data;
  return (
    <View className="coffeeBuyItem">
      <Image src={pic} className="coffeeImg" />
      <View className="coffeeInfo">
        <View className="name">{name}</View>
        <View className="decs">{decs}</View>
        <View className="price_row">
          <View className="price_wc">
            周卡<Text className="card_price">{`￥${mc_price}`}</Text>
          </View>
          <View className="originPrice">原价: ￥{price}</View>
        </View>
        <View className="speciPrice">
          <Text className="lable">到手价</Text>
          <Text className="hand_price">￥{hand_price}</Text>
        </View>
      </View>
      <View className="action">
        <Plus onClick={onChooseCoffee} />
      </View>
    </View>
  );
}
