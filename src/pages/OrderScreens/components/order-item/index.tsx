import { Image, Text, View } from "@tarojs/components";
import Taro from "@tarojs/taro";
import "./index.less";
import icons from "@/common/icon";
import { useState } from "react";

interface OrderItemProp {
  index: number;
  onChange: (index: number, sys_id: string) => void;
  item: {
    url: string;
    title: string;
    hand_price: string;
    price: string;
    capacity: string;
    patternPrice: string;
    count: number;
    less_price: string;
    less_type: number;
    total: string;
    favorableCard: string;
    favorableCoupon: string;
    detail: string;
    sys_id: string;
  };
}

function OrderItem({ item, index, onChange }: OrderItemProp) {
  const [showDetail, setShowDetail] = useState(false);

  // 0是没有优惠，1优惠卡，2优惠卷
  const isShowUseCard = item.less_type === 1;
  const isShowUseCoupon = item.less_type === 2;
  const onChangeShowDetail = () => {
    setShowDetail(!showDetail);
  };
  return (
    <View className="cupOrderItem" key={index}>
      <View className="cupDetail">
        <View className="cupImg">
          <Image className="cupImage" src={item.url} />
        </View>
        <View className="cupInfo">
          <View className="cupNameContent">
            <Text className="cupName">{item.title}</Text>
            <Text className="specialPrice">¥{item.hand_price}</Text>
          </View>
          <View className="capacityContent">
            <Text className="cupCapacity">{item.detail}</Text>
            <Text className="cupOriginalPrice">¥{item.price}</Text>
          </View>
          {/* <View className="patternContent">
            <Text className="cupPatternName">图案名称</Text>
            <Text className="cupPrice">¥{item.patternPrice}</Text>
          </View> */}
          <View className="cupCount">x1</View>
        </View>
      </View>
      {(isShowUseCard || isShowUseCoupon) && (
        <View className="favorableContent">
          <View className="favorableTotal">
            <Text className="favorableTitle">创界优惠</Text>
            <View className="favorableDetail" onClick={onChangeShowDetail}>
              <Text className="favorablePrice">-¥{item.less_price}</Text>
              <Image
                className="favorableIcon"
                src={showDetail ? icons.icon_btn_down_white : icons.icon_btn_up_white}
              />
            </View>
          </View>
          {showDetail && (
            <>
              <View className="favorableView" />
              {isShowUseCard && (
                <View className="favorableDetail">
                  <Text className="favorableCardTitle">优惠卡</Text>
                  <Text className="favorableCardPrice">-¥{item.less_price ?? 0}</Text>
                </View>
              )}
              {isShowUseCoupon && (
                <View className="favorableCoupon">
                  <Text className="favorableCardTitle">优惠券</Text>
                  <Text className="favorableCardPrice">-¥{item.less_price ?? 0}</Text>
                </View>
              )}
              <View className="favorableModify" onClick={() => onChange(index, item.sys_id)}>
                修改优惠
              </View>
            </>
          )}
        </View>
      )}
      <View className="cupItemTotalPrice">
        <Text className="itemTotalText">预计&nbsp;</Text>
        <Text className="itemTotalPrice"> ¥{item.hand_price}</Text>
      </View>
    </View>
  );
}

export default OrderItem;
