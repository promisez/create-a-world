import { View } from "@tarojs/components";
import React, { useEffect, useState } from "react";
import "./index.less";

function ActionCoffeeNum(props) {
  const { changeFn, data = 1, min = 1 } = props;
  const [actionNum, setActionNum] = useState(data);

  return (
    <View className="action-coffee-num-container">
      <View
        className={actionNum <= min ? "cut disable-cut" : "cut"}
        onClick={() => {
          if (actionNum <= min) return;
          const data1 = actionNum - 1;
          setActionNum(data1);
          changeFn(data1);
        }}
      >
        -
      </View>
      <View className="num">{actionNum}</View>
      <View
        className="add"
        onClick={(e) => {
          const data1 = actionNum + 1;
          setActionNum(data1);
          changeFn(data1);
        }}
      >
        +
      </View>
    </View>
  );
}

export default ActionCoffeeNum;
