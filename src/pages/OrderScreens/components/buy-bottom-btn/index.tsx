import { FixedView } from "@taroify/core";
import { Text, View } from "@tarojs/components";
import "./index.less";

function BuyBottomBtn({ onSpend, price = "100w", favorable = "10" }) {
  return (
    <FixedView className="buyCupFixed" position="bottom">
      <View className="totalContent">
        <View className="totalDetail">
          <Text className="totalText">应付</Text>
          <Text className="totalPrice">¥{price}</Text>
        </View>
        <Text className="totalFavorableTip">已使用优惠，共计减免¥{favorable}</Text>
      </View>
      <View className="buyCupBtn" onClick={onSpend}>
        立即支付
      </View>
    </FixedView>
  );
}

export default BuyBottomBtn;
