import { View } from "@tarojs/components";
import React from "react";
import './index.less'

interface CoffeeListContainerProps {
    title: string
    children: React.ReactNode
}
function CoffeeListContainer(props: CoffeeListContainerProps) {
  const { children, title } = props;
  return (
    <View className="coffeeListContainer" >
      <View className="title">{title}</View>
      <View className="coffeeItem">{children}</View>
    </View>
  );
}

export default CoffeeListContainer;
