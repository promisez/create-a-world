import { Checkbox, Dialog } from "@taroify/core";
import { DeleteOutlined } from "@taroify/icons";
import { ScrollView, View } from "@tarojs/components";
import React, { useEffect, useState } from "react";
import Taro from "@tarojs/taro";
import { shopCarInfo } from "@/atypes";
import { calculatePrice, calculatePriceRes } from "@/api/coffee";
import { getLoad, setLoad } from "@/common/utils";
import "./index.less";
import CarCoffeeItem from "../car-coffee-item";
import ShowShopCar from "../show-shop-car";
import CarDiscountCard from "../car-discount-card";
import { carPriceCompute, carProductNumCompute } from "./index.preset";

interface ICarPopup {
  isChecked: boolean;
  changeCardFn: (bool: boolean) => void;
  couponCard: { buy_num: number; use_num: number };
}
function CarPopup(props: ICarPopup) {
  const { isChecked, changeCardFn, couponCard } = props;
  const [isAll, setIsAll] = useState(false); // 这个初始值需要重新看下
  const [checkValue, setCheckValue] = useState<any>([]);
  const [clearCarModal, setClearCarModal] = useState(false);
  const [carNum, setCarNum] = useState(0); // 购物车中当前有多少个商品
  const [checked, setChecked] = useState(isChecked); // 优惠卡是否勾选
  const [priceCompute, setPriceCompute] = useState<calculatePriceRes>();
  const [computeArr, setComputeArr] = useState<shopCarInfo["coffee"]>([]); // 这个数据只在购物车做计算用--购物车列表

  // console.log(carNum, "carNum---carNum");

  useEffect(() => {
    const shopData = getLoad("shopCar");
    const newData: shopCarInfo = JSON.parse(shopData);
    const coffeeList = newData?.coffee ?? [];
    const currentNum = carProductNumCompute(coffeeList, setCarNum, checked); // 计算当前选中商品的数量
    // 判断是否全选
    const idArr = coffeeList?.filter((v) => v.checked)?.map((item) => item.dataId);
    console.log(isChecked, idArr?.length, coffeeList?.length);

    // 现在是有优惠卡
    if (couponCard.buy_num === couponCard.use_num) {
      if (isChecked && idArr?.length === coffeeList?.length) {
        setCheckValue([...idArr, "coupon"]);
        setIsAll(true);
      } else {
        setCheckValue(idArr);
        setIsAll(false);
      }
    } else {
      // 没有优惠卡
      setCheckValue(idArr);
      if (idArr?.length === coffeeList?.length) {
        setIsAll(true);
      } else {
        setIsAll(false);
      }
    }

    setComputeArr(coffeeList);
    if (currentNum > 0) {
      // 过滤出咖啡的id
      const computedParams = carPriceCompute(coffeeList, checked);
      calculatePriceFn(computedParams);
    }
  }, []);

  // 计算购物车价格
  function calculatePriceFn(params) {
    calculatePrice(params).then((res) => {
      setPriceCompute(res as calculatePriceRes);
    });
  }

  return (
    <View className="car-popup-container">
      <View className="checked-all-box">
        <Checkbox
          onChange={(val) => {
            if (val) {
              // 全选
              setCheckValue(computeArr?.map((i) => i.dataId));
              carProductNumCompute(computeArr, setCarNum, true); // 计算出商品的数量
              const computedParams = carPriceCompute(computeArr, true); // 重新获取价格
              calculatePriceFn(computedParams);
            } else {
              // 取消全选重新计算数量就行
              setCheckValue([]);
              carProductNumCompute([], setCarNum, false); // 计算出商品的数量
            }
            const allDataArr = computeArr?.map((k) => ({
              ...k,
              checked: val,
            }));
            setLoad(
              "shopCar",
              JSON.stringify({ coffee: allDataArr, cup: [], image: [], isChecked: val })
            );
            setChecked(val);
            setIsAll(val);
          }}
          checked={isAll}
          name="all"
          shape="square"
          size={16}
          className="checked-all"
        >
          已选购商品({carNum}件)
        </Checkbox>
        <View className="checked-num" onClick={() => setClearCarModal(true)}>
          <DeleteOutlined /> 清空购物车
        </View>
      </View>
      <ScrollView className="scrollview" scrollY scrollWithAnimation scrollTop={0}>
        <Checkbox.Group
          className="checked-group"
          value={checkValue}
          onChange={(val) => {
            // 只用来判断是否全选
            setCheckValue(val);
            if (val.length === computeArr?.length + 1) {
              setIsAll(true);
            } else {
              setIsAll(false);
            }
          }}
        >
          {couponCard.buy_num === couponCard.use_num && (
            <Checkbox
              name="coupon"
              shape="square"
              size={16}
              checked={checked}
              onChange={(bool) => {
                changeCardFn(bool);
                setChecked(bool);
                // 这个地方需要看一下勾选的逻辑
                if (bool && !checkValue.includes("coupon")) {
                  setCheckValue((res) => [...res, "coupon"]);
                  carProductNumCompute(computeArr, setCarNum, true); // 计算出商品的数量
                  const computedParams = carPriceCompute(computeArr, true); // 重新获取价格
                  calculatePriceFn(computedParams);
                } else {
                  setCheckValue((res) => res.filter((k) => k !== "coupon"));
                  carProductNumCompute(computeArr, setCarNum, false); // 计算出商品的数量
                  const computedParams = carPriceCompute(computeArr, false); // 重新获取价格
                  calculatePriceFn(computedParams);
                }
                setLoad(
                  "shopCar",
                  JSON.stringify({ coffee: computeArr, cup: [], image: [], isChecked: bool })
                );
              }}
              key="coupon"
              className="coffee-checkbox"
            >
              <CarDiscountCard />
            </Checkbox>
          )}

          {computeArr?.map((item, index) => {
            return (
              <Checkbox
                name={item.dataId} // 不能用这个做id判断是否选中，因为可能存在相同的商品不同的类型，这里就会与相同的 model_id
                shape="square"
                size={16}
                onChange={(bool) => {
                  if (bool && !checkValue.includes(item.dataId)) {
                    // 勾选--需要重新计算数量，和购物车的商品列表，和价格
                    setCheckValue((res) => [...res, item.dataId]);
                    const data = computeArr.map((k) => {
                      if (k.dataId !== item.dataId) {
                        return k;
                      }
                      return { ...k, checked: bool };
                    });
                    setComputeArr(data);
                    setLoad(
                      "shopCar",
                      JSON.stringify({
                        coffee: data,
                        cup: [],
                        image: [],
                        isChecked: bool,
                      })
                    );
                    console.log(data, "勾选", "34534534");
                    carProductNumCompute(data, setCarNum, checked); // 计算出商品的数量
                    const computedParams = carPriceCompute(data, checked); // 重新获取价格
                    calculatePriceFn(computedParams);
                  } else {
                    // 取消勾选--需要重新计算数量，和购物车的商品列表的勾选，和价格，不需要重置列表的展示
                    const checkIdArr = checkValue.filter((k) => k !== item.dataId);
                    console.log(checkIdArr, checkValue, "打印一下选择");

                    setCheckValue(checkIdArr);
                    const data = computeArr.map((k) => {
                      if (k.dataId !== item.dataId) {
                        return k;
                      }
                      return { ...k, checked: bool };
                    });
                    console.log(data, "data---取消勾选");
                    setLoad(
                      "shopCar",
                      JSON.stringify({
                        coffee: data,
                        cup: [],
                        image: [],
                        isChecked: checked,
                      })
                    );
                    setComputeArr(data);
                    carProductNumCompute(data, setCarNum, checked); // 计算出商品的数量
                    const computedParams = carPriceCompute(data, checked); // 重新获取价格
                    calculatePriceFn(computedParams);
                  }
                }}
                key={index}
                className="coffee-checkbox"
              >
                <CarCoffeeItem
                  data={item}
                  changeNumFn={(num) => {
                    const computeData = computeArr
                      ?.map((v) => {
                        if (v.dataId === item.dataId) {
                          return { ...v, currentNum: num };
                        }
                        return v;
                      })
                      ?.filter((k) => k.currentNum > 0);
                    if (!computeData?.length) {
                      setCheckValue([]);
                      setIsAll(false);
                    }
                    carProductNumCompute(computeData, setCarNum, checked); // 计算出商品的数量
                    setComputeArr(computeData);
                    const computedParams = carPriceCompute(computeData, checked);
                    calculatePriceFn(computedParams);
                    setLoad(
                      "shopCar",
                      JSON.stringify({
                        coffee: computeData,
                        cup: [],
                        image: [],
                        isChecked: checked,
                      })
                    );
                  }}
                />
              </Checkbox>
            );
          })}
        </Checkbox.Group>
        <View className="bottom-gap" />
      </ScrollView>

      <View className="bottomButton">
        <ShowShopCar
          onShowCar={() => {}}
          carNum={carNum}
          checkedCard={checked}
          priceData={priceCompute as calculatePriceRes}
          toSettlementFn={() => {
            // 点击去结算 --- 这个是购物车弹出层里面的去结算
            // 过滤出已经选择的产品，去了结算界面就把没选中的直接去除
            const newData = computeArr?.filter((v) => v.checked);
            // 后期有杯子还需要修改
            setLoad(
              "shopCar",
              JSON.stringify({ coffee: newData, cup: [], image: [], isChecked: checked })
            );
            Taro.navigateTo({
              url: "/pages/OrderScreens/buycoffee/index",
            });
          }}
        />
      </View>
      <Dialog open={clearCarModal} onClose={setClearCarModal} className="clear-car-modal">
        <Dialog.Content>
          <View className="clear-tip">确定清空购物车？</View>
          <View className="btn">
            <View className="cancel" onClick={() => setClearCarModal(false)}>
              取消
            </View>
            <View
              className="sure"
              onClick={() => {
                // 清空购物车
                setLoad(
                  "shopCar",
                  JSON.stringify({ cup: [], image: [], coffee: [], isChecked: false })
                );
                setCarNum(0);
                setComputeArr([]);
                setClearCarModal(false);
                setIsAll(false);
                setChecked(false);
                changeCardFn(false);
              }}
            >
              确定
            </View>
          </View>
        </Dialog.Content>
      </Dialog>
    </View>
  );
}

export default CarPopup;
