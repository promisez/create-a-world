/**
 * 购物车价格计算
 * @return  computedParams
 */
export function carPriceCompute(list, checked) {
  const coffeeIdList =
    list
      ?.map((k) => {
        return Array(k.currentNum).fill({
          coffee_id: k?.model_id,
        });
      })
      ?.flat(1) ?? [];
  return {
    list: coffeeIdList,
    isChecked: checked,
  };
}

/**
 * 计算出商品的总数
 * @param list 商品列表
 * @param setNum 计算后累加的值
 * @return 商品总数
 */
export function carProductNumCompute(list, setNum, checked) {
  const num = list?.reduce((a, b) => {
    if (b.checked) {
      return a + b?.currentNum;
    }
    return a;
  }, 0);
  const num1 = checked ? num + 1 : num;
  console.log(num, num1, "看看打印的数量----");

  setNum(num1);
  return num1;
}
