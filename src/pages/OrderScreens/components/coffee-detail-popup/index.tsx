import { ScrollView, View, Image } from "@tarojs/components";
import React, { useState } from "react";
import { currentCarCoffeeInfo } from "@/atypes";
import Taro from "@tarojs/taro";
import { getLoad, setLoad } from "@/common/utils";
import "./index.less";
import RadioCoffeeTag from "../radio-coffee-tag";
import ActionCoffeeNum from "../action-num";
import { carPriceCompute } from "../car-popup/index.preset";

function CoffeeDetailPopup(props) {
  const { onClose, detail } = props;

  const { option, base_ice, base_sweet } = detail;
  // 选择类型的映射
  const typeMap = [
    {
      title: "温度",
      type: "ice",
      dataArr: option?.ice,
      defaultValue: base_ice,
    },
    {
      title: "甜度",
      type: "sweet",
      dataArr: option?.sweet,
      defaultValue: base_sweet,
    },
  ];
  // 默认值
  const defaultData = {
    ice: "热",
    sweet: "免糖",
    currentNum: 1,
    dataId: Date.now(),
    checked: true,
    ...detail,
  };
  const [currentOrderInfo, setCurrentOrderInfo] = useState<currentCarCoffeeInfo>(defaultData);

  return (
    <View className="coffee-detail-container">
      <View className="coffee-detail-popup">
        <ScrollView className="scrollview" scrollY scrollWithAnimation scrollTop={0}>
          <Image src={detail?.title_pic} className="detail-img" />
          <View className="detail-choose-type">
            <View className="name">{detail?.name}</View>
            {typeMap?.map((item, index) => {
              return (
                <View className="choose-type-contant" key={index}>
                  <View className="title">{item.title}</View>
                  <RadioCoffeeTag
                    data={item.dataArr}
                    changeFn={(data) => setCurrentOrderInfo((r) => ({ ...r, [item.type]: data }))}
                  />
                </View>
              );
            })}
          </View>
          <View className="product-detail-show">
            <View className="title">商品详情</View>
            <View className='img'>
              <Image src={detail?.detail_pic} mode='widthFix' />
            </View>
          </View>
          <View className="bottom-gap" />
        </ScrollView>
        <View className="bottomButton">
          <View className="price-calculation">
            <View className="price-show-contant">
              <View className="price-show">
                <View className="expected">预计</View>
                <View className="current-price">
                  ￥{currentOrderInfo?.currentNum * detail.hand_price}
                </View>
                {/* <View className="original-price">原价￥{detail?.price}</View> */}
              </View>
              {/* <View className="calculate-detail">{`杯子¥0+打印费¥0+${detail.name}¥${
                currentOrderInfo?.currentNum * detail.hand_price
              }`}</View> */}
            </View>
            <View className="action-num">
              <ActionCoffeeNum
                changeFn={(num) => setCurrentOrderInfo((r) => ({ ...r, currentNum: num }))}
              />
            </View>
          </View>
          <View className="action">
            <View
              className="add-shop"
              onClick={() => {
                const shopData = getLoad("shopCar");
                const newData = JSON.parse(shopData);
                // 直接买咖啡
                if (!newData.coffee.length) {
                  setLoad(
                    "shopCar",
                    JSON.stringify({
                      coffee: [currentOrderInfo],
                      cup: {},
                      image: {},
                      isChecked: newData.isChecked,
                    })
                  );
                  onClose();
                  return;
                }
                // 按照流程买咖啡
                // 需要看商品model_id、选择的 温度、甜度、奶 是不是一样的
                // 如果一样就数量对应的增减
                // 如果不一样就需要加一项
                // 需要重新计算
                const { sweet, ice, currentNum, model_id } = currentOrderInfo;
                let count = 0;
                newData?.coffee?.forEach((item) => {
                  // model_id 甜度 温度 都相等时，数量 相加
                  if (item.model_id === model_id && item.sweet === sweet && item.ice === ice) {
                    item.currentNum += currentNum;
                    count++; // 每次进入购物车只会加一次
                  }
                });
                // id 甜度 温度 有一个不相等时，加一项
                if (count < 1) {
                  newData?.coffee?.push({ ...currentOrderInfo, dataId: Date.now() });
                }

                setLoad("shopCar", JSON.stringify({ ...newData }));
                onClose();
              }}
            >
              加入购物车
            </View>
            <View
              className="buy"
              onClick={() => {
                const payload = carPriceCompute(
                  [currentOrderInfo],
                  getLoad("shop-payload").isChecked
                );
                setLoad("shop-payload", payload);
                setLoad(
                  "shopCar",
                  JSON.stringify({
                    coffee: [currentOrderInfo],
                    cup: {},
                    image: {},
                    isChecked: false,
                  })
                );
                Taro.navigateTo({ url: "/pages/OrderScreens/buycoffee/index" });
              }}
            >
              立即购买
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

export default CoffeeDetailPopup;
