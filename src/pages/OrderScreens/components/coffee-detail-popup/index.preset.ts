// 选择类型的映射
export const typeMap = [
  {
    title: "温度",
    type: "temperature",
  },
  {
    title: "甜度",
    type: "sweetness",
  },
  {
    title: "奶",
    type: "milk",
  },
];
