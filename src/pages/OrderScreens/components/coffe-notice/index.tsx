import { NoticeBar } from "@taroify/core";
import { Text, View } from "@tarojs/components";
import Taro from "@tarojs/taro";
import React from "react";
import { Clear } from "@taroify/icons";
import { useEffect, useState } from "react";
import "./index.less";

function CoffeeNotice() {
  const [isScroll, setIsScroll] = useState(false);
  const [viewInfo, setViewInfo] = useState(0);
  const [textInfo, setTextInfo] = useState(0);
  const [isShow, setIsShow] = useState(true);
  const getInfo = (id, setInfo) => {
    Taro.createSelectorQuery()
      .select(id)
      .boundingClientRect()
      .exec((res) => {
        setInfo(res[0]?.height);
      });
  };
  useEffect(() => {
    if (!viewInfo) getInfo("#noticeBar", setViewInfo);
    if (!textInfo) getInfo("#scrolltext", setTextInfo);

    if (viewInfo && textInfo) {
      console.log(textInfo, "textInfo", viewInfo, "viewInfo");

      if (textInfo >= viewInfo) {
        setIsScroll(true);
      }
    }
  }, [textInfo, viewInfo]);

  const text = "温馨提示 ：热门款中每系出溢出大幅度发溢出61314";
  console.log(isShow, isScroll, "isShowisShowisShow");

  if (!isShow) return null;

  if (!isScroll) {
    return (
      <View id="noticeBar" className="coffeeNoticebar">
        <Text numberOfLines={1} id="scrolltext">
          {text}
        </Text>
        <Clear className="clearIcon" size="12" onClick={() => setIsShow(false)} />
      </View>
    );
  }
  return (
    <NoticeBar
      scrollable={true}
      key="NoticeBar"
      onClick={() => setIsShow(false)}
      className="coffeeNoticebar"
    >
      <Text id="noticeBar">{text}</Text>
      <NoticeBar.Action>
        <Clear className="clearIcon" size="12" />
      </NoticeBar.Action>
    </NoticeBar>
  );
}

export default React.memo(CoffeeNotice);
