// 温度
export const data1 = [
  {
    id: "01",
    text: "热",
  },
  {
    id: "02",
    text: "免冰",
  },
  {
    id: "03",
    text: "三分冰",
  },
  {
    id: "04",
    text: "五分冰",
  },
  {
    id: "05",
    text: "七分冰",
  },
  {
    id: "06",
    text: "全冰",
  },
];

// 糖量
export const data2 = [
  {
    id: "01",
    text: "免糖",
  },
  {
    id: "02",
    text: "三分糖",
  },
  {
    id: "03",
    text: "五分糖",
  },
  {
    id: "04",
    text: "七分糖",
  },
  {
    id: "5",
    text: "全糖",
  },
];

// 奶量
export const data3 = [
  {
    id: "01",
    text: "无奶",
  },
  {
    id: "02",
    text: "单份奶",
  },
  {
    id: "03",
    text: "双份奶",
  },
];

export const chooseTypeMap = {
  temperature: data1,
  sweetness: data2,
  milk: data3,
};
