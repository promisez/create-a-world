import { View } from "@tarojs/components";
import React, { useEffect, useState } from "react";
import ChooseCoffeeTag from "../choose-coffee-tag";
// import { chooseTypeMap } from "./index.preset";
import "./index.less";

function RadioCoffeeTag(props) {
  const { changeFn, data = [] } = props;
  const [value, setValue] = useState(data?.[0]); // 默认值暂时为第0项，后面再定

  useEffect(() => {
    changeFn(value);
  }, [value, data]);

  return (
    <View className="radio-coffee-tag-container">
      {data?.map((item, index) => {
        return (
          <View key={index} onClick={() => setValue(item)} className="type-item">
            <ChooseCoffeeTag current={value === item} text={item} />
          </View>
        );
      })}
    </View>
  );
}

export default RadioCoffeeTag;
