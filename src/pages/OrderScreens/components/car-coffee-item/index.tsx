import { View, Image } from "@tarojs/components";
import React from "react";
import "./index.less";
import ActionCoffeeNum from "../action-num";

function CarCoffeeItem(props) {
  const { data, changeNumFn } = props;
  return (
    <View className="car-coffee-item-container" onClick={(e) => e.stopPropagation()}>
      <Image src={data.pic} className="img" />
      <View className="car-item-info">
        <View className="item-name">{data.name}</View>
        <View className="item-detail">
          <View className="coffee-style">{`${data.ice}+${data.sweet}`}</View>
          <View className="coffee-price">
            {/* <View className="received-price">到手价</View>
            <View className="price">{`¥${data.price}`}</View> */}
            <View className="original-price">{`原价¥${data.price}`}</View>
          </View>
        </View>
      </View>
      <ActionCoffeeNum data={data.currentNum} changeFn={(num) => changeNumFn(num)} min={0} />
    </View>
  );
}

export default CarCoffeeItem;
