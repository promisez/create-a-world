import icons from "@/common/icon";
import { Checkbox } from "@taroify/core";
import { View, Image, Text } from "@tarojs/components";
import React from "react";
import "./index.less";
import Taro from "@tarojs/taro";

interface CountCard {
  checked?: boolean;
  onChange?: (value: boolean) => void;
  showSelect?: boolean;
}
function CountCard({ checked, onChange, showSelect }: CountCard) {
  const goRule = () => {
    Taro.navigateTo({
      url: "/pages/MeScreens/cardRule/index",
    });
  };
  return (
    <View className={`buyCountCard ${showSelect ? "" : "buyCountCardMy"}`}>
      <View className="bugCardLeft">
        <View className="cardImageContent">
          <Image className="cardImage" src={icons.icon_buy_card} />
        </View>
        <View className="buyCardContent">
          <View className="buyCardInfo">
            <Text className="buyCardTitle">购买次卡</Text>
            {showSelect && (
              <Text className="buyCardRule" onClick={goRule}>
                优惠规则{">"}
              </Text>
            )}
          </View>
          <Text className="buyCardTips">购买后即可享受7次优惠</Text>
        </View>
      </View>
      {showSelect ? (
        <Checkbox
          checked={checked}
          className="cardCheckbox"
          size={20}
          shape="square"
          onChange={onChange}
        />
      ) : (
        <Text className="buyCardRuleRight" onClick={goRule}>
          优惠规则{" >"}
        </Text>
      )}
    </View>
  );
}

export default CountCard;
