import { View, Text } from "@tarojs/components";
import React, { useEffect, useState } from "react";
import { Location } from "@taroify/icons";
import "./index.less";
import Taro from "@tarojs/taro";
import { distanceConverFn, getLoad } from "@/common/utils";
interface RegionProp {
  address?: string;
  distance?: number;
  onClick?: () => void;
  name?: string;
  device_id?: string;
}
function OrderRegion(props: RegionProp) {
  const { onClick, ...rest } = props;
  const [regionInfo, setRegionInfo] = useState<Omit<RegionProp, "onClick">>({});
  useEffect(() => {
    if (rest.device_id) {
      const list = getLoad("region_list");
      const info = list?.find((item) => item.device_id === rest.device_id);
      setRegionInfo(info);
      return;
    }
    if (rest.address) {
      setRegionInfo(rest);
      return;
    }
    const region = getLoad("region");

    if (region) {
      setRegionInfo(region);
    }
  }, [props]);
  const { address = "当前位置", distance = 0, name = "具体位置" } = regionInfo;

  return (
    <View className="stepContainer" onClick={onClick}>
      <View className="region">
        <Location className="icon" color="red" size={16} />
        <View className="regionContent">
          <View className="regionInfo">
            <Text className="regionName">{address}</Text>
            <Text className="regionTip"></Text>
          </View>
          <Text className="regionAddress">{name}</Text>
        </View>
      </View>
    </View>
  );
}

export default OrderRegion;
