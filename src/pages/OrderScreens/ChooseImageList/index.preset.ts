const hotList: any[] = []
const collectList: any[] = []
const uploadList: any[] = []
const dataObj: Record<string, any> = {}

Array(30).fill(0).forEach((item, index) => {
  hotList.push({
    url: 'https://file.psd.cn/2022/02-18/8d7eccb968c7bbc1d600d7e41e56f7d7.jpg',
    title: '我是热门款标题'
  })
  // collectList.push({
  //   url: 'https://file.psd.cn/2022/02-18/8d7eccb968c7bbc1d600d7e41e56f7d7.jpg',
  //   title: '我是收藏款标题'
  // })
  uploadList.push({
    url: 'https://file.psd.cn/2022/02-18/8d7eccb968c7bbc1d600d7e41e56f7d7.jpg',
    title: '我是上传款标题'
  })
  dataObj.hot = hotList
  dataObj.collect = collectList
  dataObj.upload = uploadList
})

export default dataObj

export const tabConfig = [
  {
    showCollect: false,
    showLike: true,
    showUseCount: true,
    showTag: false
  },
  {
    showCollect: true,
    showLike: true,
    showUseCount: true,
    showTag: true

  },
  {
    showCollect: false,
    showLike: false,
    showUseCount: false,
    showTag: false,

  }
]