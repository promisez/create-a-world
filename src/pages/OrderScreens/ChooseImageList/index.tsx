import ImageCut from "@/components/image-cut";
import ImageDetail from "@/components/image-detail";
import NavTopBar from "@/components/nav-bar";
import ShapeBadge from "@/components/shape-badge";

import icons from "@/common/icon";
import Button from "@taroify/core/button";
import { View, Image, Text } from "@tarojs/components";
import Taro from "@tarojs/taro";
import { useCallback, useEffect, useMemo, useState } from "react";
import CoffeeNotice from "../components/coffe-notice";
import OrderStepHeader from "../components/order-header";
import "./index.less";
import dataObj, { tabConfig } from "./index.preset";
import { Backdrop, Popup } from "@taroify/core";
import PopUpHeader from "@/components/popup-header";
import Mall from "@/pages/mall";
import { getLogoList } from "@/api/order";

const titleArr = [
  {
    title: "热门款",
    text: "免费打印",
    type: "hot",
    color: "#E6051F",
    icon: icons.icon_hot,
    actIcon: icons.icon_hot_active,
  },
  {
    title: "收藏款",
    text: "给钱2块",
    type: "collect",
    color: "#FF9600",
    icon: icons.icon_collect,
    actIcon: icons.icon_collect_active,
  },
  {
    title: "上传款",
    text: "打印费￥2",
    type: "upload",
    color: "#3399EB",
    icon: icons.icon_upload,
    actIcon: icons.icon_upload_active,
  },
];
function ChooseImageList() {
  const [actIndex, setActIndex] = useState(0);
  const [actImgIndex, setActImgIndex] = useState<undefined | number>();
  const [datalist, setDatalist] = useState<any[]>(dataObj["hot"]);
  const itemConfig = useMemo(() => tabConfig[actIndex], [actIndex]);
  const [isOpen, setIsOpen] = useState(false);
  const onActive = async (index, type) => {
    setActIndex(index);
    setActImgIndex(undefined);
    if (type === "hot") {
      const res = await getLogoList();
      console.log(res, "resres");

      setDatalist(dataObj["hot"]);
      return;
    }

    const data = JSON.parse(JSON.stringify(dataObj[type]));
    if (index === 2) {
      data.unshift({ isUpload: true });
    }
    setDatalist(data);
    // loading , request

    // setDatalist(res)
  };
  const goNext = () => {
    if (isDisable) return;
    Taro.navigateTo({
      url: "/pages/OrderScreens/coffee/index",
    });
  };
  const goBugCup = () => {
    if (isDisable) return;

    Taro.navigateTo({
      url: "/pages/OrderScreens/buycup/index",
    });
  };
  const goCollect = () => {
    setIsOpen(true);
  };
  const onCut = (status) => {
    if (status) {
    }
    console.log(status);
  };
  const RenderList = useCallback(() => {
    return datalist?.map((v, i) =>
      v.isUpload ? (
        <View key={i} className="show-image-box cut-img">
          <ImageCut onCut={onCut} />
        </View>
      ) : (
        <View key={i} className="show-image-box">
          <ImageDetail
            index={i}
            actIndex={actImgIndex}
            onClick={() => setActImgIndex(i)}
            showPopUp={actIndex === 0}
            data={v}
            {...itemConfig}
          />
        </View>
      )
    );
  }, [datalist, actImgIndex, itemConfig]);

  const Empty = useCallback(() => {
    return (
      <View className="emptyContainer">
        <View className="emptyContent">
          <View className="collectEmptyImg">
            <Image className="emptyImage" src={icons.collect_empy}></Image>
          </View>
          <View className="goToCollect" onClick={goCollect}>
            马上去收藏
          </View>
        </View>
        <View className="emptyText">
          <Text>您还没有任何收藏</Text>
          <Text>进入共享大厅收藏您喜欢</Text>
        </View>
      </View>
    );
  }, []);

  const isDisable = useMemo(() => typeof actImgIndex !== "number", [actImgIndex]);
  useEffect(() => {
    onActive(0, "hot");
  }, []);

  return (
    <View className="choose-image-list-content">
      <NavTopBar />
      <OrderStepHeader step={2} title="选图案" />
      <View className="shape-container">
        {titleArr?.map((item, index) => {
          return (
            <ShapeBadge
              {...item}
              onClick={() => onActive(index, item.type)}
              actIndex={actIndex}
              index={index}
              key={index}
            />
          );
        })}
      </View>
      <CoffeeNotice />
      <View className="shape-show-content">{datalist.length ? <RenderList /> : <Empty />}</View>
      <Backdrop
        className="collectBackdrop"
        open={isOpen}
        closeable
        onClose={() => setIsOpen(false)}
      >
        <Popup className="collectPopup" open={isOpen} placement="bottom" style={{ height: "90%" }}>
          <PopUpHeader onClick={() => setIsOpen(false)} />
          <Mall />
        </Popup>
      </Backdrop>
      <View style={{ height: 48 }} />
      <View className="bottomButton">
        <Button className={`buyCup ${isDisable ? "buyCup-disabled" : ""}`} onClick={goBugCup}>
          购买杯子
        </Button>
        <Button onClick={goNext} className={`buyCoffee ${isDisable ? "buyCoffee-disabled" : ""}`}>
          选购咖啡
        </Button>
      </View>
    </View>
  );
}

export default ChooseImageList;
