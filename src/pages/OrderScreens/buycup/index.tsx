import Header from "@/components/common-header";
import NavTopBar from "@/components/nav-bar";
import { View } from "@tarojs/components";
import Taro from "@tarojs/taro";
import { useState } from "react";
import BuyBottomBtn from "../components/buy-bottom-btn";
import OrderItem from "../components/order-item";
import "./index.less";
import defaultData from "./index.preset";

function BuyCup() {
  const [list, setList] = useState(defaultData);
  const onSpend = () => {
    Taro.showToast({
      title: "没钱",
      icon: "error",
      duration: 1000,
    });
  };
  return (
    <View className="buyCupContainer">
      <NavTopBar />
      <Header title="确认订单" />
      <View className="cupOrderList">
        {list.map((item, index) => {
          return <OrderItem onChange={() => {}} key={index} item={item} index={index} />;
        })}
        <BuyBottomBtn onSpend={onSpend} price="100w" favorable="10" />
      </View>
    </View>
  );
}

export default BuyCup;
