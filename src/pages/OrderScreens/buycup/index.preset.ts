const defaultData: any[] = []
Array(2).fill(1).forEach((item, index) => {
  defaultData.push({
    url: 'https://file.psd.cn/2022/02-18/8d7eccb968c7bbc1d600d7e41e56f7d7.jpg',
    title: '杯子名称',
    capacity: '120ml',
    originalPrice: '12.00',
    specialPrice: '10.00',
    patternName: '图案名称',
    patternPrice: '10.00',
    count: 1,
    favorable: '4',
    total: '16.00'
  })
})

export default defaultData