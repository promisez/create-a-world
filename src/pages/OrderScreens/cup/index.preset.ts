export const list: Array<any> = [];
Array(10).fill(null).forEach((item, index) => {
  list.push({
    url: "https://file.psd.cn/2022/02-18/8d7eccb968c7bbc1d600d7e41e56f7d7.jpg",
    title: numberToChinese(index + 1) + "次性杯子",
    capacity: "100ml",
    description: "杯子描述文案杯子描述文案杯子描述文案杯子",
    price: "10元",
  })
})
export function numberToChinese(num) {
  const chineseNums = [
    '一', '二', '三', '四', '五', '六', '七', '八', '九', '十'
  ];

  if (num <= 10) {
    return chineseNums[num - 1];
  } else if (num < 20) {
    const ones = num % 10;
    let result = '十';
    if (ones !== 0) {
      result += chineseNums[ones - 1];
    }
    return result;
  } else if (num < 100) {
    const tens = Math.floor(num / 10);
    const ones = num % 10;
    let result = chineseNums[tens - 1] + '十';
    if (ones !== 0) {
      result += chineseNums[ones - 1];
    }
    return result;
  } else {
    return '目前只支持1到99的转换';
  }
}
