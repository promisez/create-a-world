import { getCupList } from "@/api/order";
import NavTopBar from "@/components/nav-bar";
import { Button, Image, Text, View } from "@tarojs/components";
import Taro from "@tarojs/taro";
import { useEffect, useState } from "react";
import OrderStepHeader from "../components/order-header";
import "./index.less";
import { list } from "./index.preset";
import { CupList } from "@/atypes";
import { getLoad, setLoad } from "@/common/utils";

function Cup() {
  const [actIndex, setActIndex] = useState<number>();
  const [cupList, setCupList] = useState<CupList[]>([]);

  async function getCupListFn() {
    const data = await getCupList();
    setCupList(data);
  }

  useEffect(() => {
    getCupListFn();
  }, []);
  const goNext = () => {
    if (actIndex === undefined) return;
    const shopCar = JSON.parse(getLoad("shopCar"));
    shopCar.cup = cupList[actIndex];
    // 这个地方可能需要加一个字段 isChecked 判断有没有勾选优惠卡
    setLoad("shopCar", JSON.stringify(shopCar));

    Taro.navigateTo({
      // url: "/pages/order/ChooseImageList/index",
      url: "/pages/OrderScreens/coffee/index",
    });
  };
  return (
    <View className="cupContainer">
      <NavTopBar />
      <OrderStepHeader step={1} />
      <View className="cupListContent">
        {cupList.map((item, index) => {
          return (
            <View
              onClick={() => setActIndex(index)}
              className={`item ${actIndex === index ? "active" : ""}`}
              key={index}
            >
              <Image className="image" src={item.pic} />
              <View className="content">
                <Text className="title">{item.name}</Text>
                <Text className="capacity">{item.cap}ml</Text>
                <Text className="capacity">{item.desc || "description"}</Text>
                <Text className="price">{item.price}元</Text>
              </View>
            </View>
          );
        })}
      </View>
      <View style={{ height: 48 }} />
      <View
        className={`buttonContainer ${actIndex === undefined ? "buttonContainer-disabled" : ""}`}
        onClick={goNext}
      >
        <Button className="button"> 下一步: 选图案</Button>
      </View>
    </View>
  );
}

export default Cup;
