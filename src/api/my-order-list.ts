import { ResProp, coffeeItem } from "@/atypes";
import { request } from "@/common/request";
import { ICoupon, ICup, IFormula } from "./coffee";

interface ICardInfo {
  buy_price: string;
  discount: string;
  name: string;
  num: number;
  range: null;
  sys_id: string;
  value: string;
}

interface coffeeInfo {
  card: ICardInfo;
  coffee: coffeeItem;
  coffee_model_id: string;
  cool: string;
  coupon: ICoupon;
  cup: ICup;
  cup_model_id: string;
  formula: IFormula;
  id: string;
  less_type: number;
  logo_series_id: string;
  logo_url: string;
  state: number;
  sweet: string;
}
interface IMakeInfo {
  coffees: coffeeInfo[];
  device_id: string;
  is_query: boolean;
  order_id: string;
  owner_id: string;
  use_card: boolean;
}

interface IPayInfo {
  List: coffeeInfo[];
  card: ICardInfo;
  card_add: number;
  card_sub: number;
  final_price: string;
  less_price: string;
  source_price: string;
}

export interface orderListRes {
  DisplayState: number;
  TakeNum: number;
  FinishTime: number;
  MakeInfo: IMakeInfo;
  MakeState: { Coffees: any[] };
  OrderTime: number;
  PayInfo: IPayInfo;
  PayState: number;
  OrderTakeNum:number;
  TakeInfo: {
    order_id: string;
    qr_param: string;
  };
}
export async function getMyOrderList(index) {
  const data = await request<ResProp<orderListRes[]>>({
    url: `mini/MyOrder`,
    method: "POST",
    data: {
      page: index,
      size: 10,
    },
  });
  if (data.Code !== 200) {
    return [];
  }
  return data?.Data ?? [];
}
