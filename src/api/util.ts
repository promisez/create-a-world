import Taro from "@tarojs/taro"

// toast 
export const toast = (title:string) => {
  Taro.showToast({
    title,
    icon: 'none',
    duration: 1000
  })
}