import { ResProp } from "@/atypes";
import { request } from "@/common/request";

export async function getCommonInfo() {
  const res = await request<ResProp<any>>({
    url: "/mini/PublicInfo",
    method: "GET",
  });

  if (res?.Code !== 200) {
    return [];
  }
  return res?.Data;
}

// 

export async function getCoupon() {
  const res = await request<ResProp<any>>({
    url: "/mini/EveryDayTake",
    method: "GET",
  });

  if (res?.Code !== 200) {
    return [];
  }
  return res?.Data;
}

// 获取首页banner图
export async function getBanner() {
  const res = await request<ResProp<any>>({
    url: "/mini/Material",
    method: "GET",
  });

  if (res?.Code !== 200) {
    return [];
  }
  return res?.Data;
}

// 获取手机号
export async function getMobile(data) {
  const res = await request<ResProp<any>>({
    url: "mini/getMobile",
    method: "POST",
    data
  });

  if (res?.Code !== 200) {
    return [];
  }
  return res?.Data;
}