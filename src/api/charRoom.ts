import { ResProp } from "@/atypes/common";
import { request } from "@/common/request";

export async function getCharRoomList() {
  const res = await request<ResProp<any>>({
    url: "Mini/History/Room",
  });
  if (res.Code !== 200) {
    return [];
  }
  return res.Data;
}

export async function deleteCharRoomItem(id: string) {
    const res = await request<ResProp<any>>({
      url: "Mini/History/RoomDelete",
      method: "POST",
      data: {
        room_id: id
      }
    });
    if (res.Code !== 200) {
      return [];
    }
    return res.Data;
  }
