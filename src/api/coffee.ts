import { CoffeeTypeList, ResProp } from "@/atypes";
import { request } from "@/common/request";
import Taro from "@tarojs/taro";
import { toast } from "./util";
import { getLoad, setLoad } from "@/common/utils";

// 获取咖啡类型列表
export async function getCoffeeTypeList(id: string) {
  const data = await request<ResProp<CoffeeTypeList[]>>({
    url: `Mini/CoffeeClassList?deviceId=${id}`,
  });
  if (data.Code !== 200) {
    return [];
  }
  return data;
}

export interface calculatePriceParams {
  list: { coffee_id: string; coupon_id?: string }[];
  isChecked: boolean;
}

export interface ICoupon {
  discount: string;
  expire_time: number;
  grant_time: number;
  id: string;
  name: string;
  num: number;
  owner: string;
  range: number;
  sys_id: string;
  value: string;
}

export interface ICup {
  cap: string;
  color: string;
  desc: string;
  model_id: string;
  name: string;
  pic: string;
  price: string;
}
export interface IFormula {
  beverage: number;
  ice_number: number;
  id: string;
  sweet: string;
  tea: number;
}
interface coffeeInfo {
  base_ice: string;
  cool_model_id: string;
  hand_price: string;
  hot_model_id: string;
  less_price: string;
  mc_price: string;
  model_id: string;
  name: string;
  pic: string;
  price: string;
}
export interface calculatePriceListRes {
  coffee: coffeeInfo;
  coffee_model_id: string;
  cool: string;
  coupon: ICoupon;
  cup: ICup;
  cup_model_id: string;
  formula: IFormula;
  id: string;
  logo_series_id: string;
  logo_url: string;
  state: 0;
  sweet: string;
  less_type: number;

}
export interface calculatePriceRes {
  List: calculatePriceListRes[];
  final_price: string;
  less_price: string;
  source_price: string;
}
// 计算咖啡购物车的价格
export async function calculatePrice(params: calculatePriceParams) {

  setLoad("shop-payload", params);
  const device = getLoad('region')
  const { list, isChecked } = params;
  const res = await request<ResProp<calculatePriceRes>>({
    url: "mini/OrderCompute",
    method: "POST",
    data: {
      use_card: isChecked,
      makes: list,
      device_id: device?.device_id ?? ''
    },
  })
  return res?.Data;
}


// 优惠卡接口
export async function getCouponCard() {
  const res = await request<ResProp<any>>({
    url: "mini/MyCouponCard",

  });
  if (res.Code !== 200) {
    return [];
  }
  return res.Data;
}

// 优惠券列表
export async function getCouponList() {
  const res = await request<ResProp<any>>({
    url: "mini/SysCoupons",

  });
  if (res.Code !== 200) {
    return [];
  }
  return res.Data;
}

// 支付
export async function pay(params) {
  const res = await request<ResProp<any>>({
    url: "mini/OrderCommit",
    method: "POST",
    data: params,
  });
  return res?.Data
}
