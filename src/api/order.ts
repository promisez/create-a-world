import { request } from "@/common/request";
import { CupList, LocationList, LogoList, ResProp, UploadProp } from "@/atypes";
import { toast } from "./util";
import Taro from "@tarojs/taro";



// 获取设备位置信息列表
export async function getDeviceList(params) {
  const res = await request<ResProp<LocationList[]>>({
    url: "mini/MachineList",
    method: 'POST',
    data: {
      longitude: String(params.longitude), 
      latitude: String(params.latitude)
    }
  }).catch((err) => {
    toast("获取设备位置信息列表失败，请重试")
  });

  if (res?.Code !== 200) {
    return [];
  }
  return res?.Data;
}


// 获取杯子列表
export async function getCupList() {
  const res = await request<ResProp<CupList[]>>({
    url: "mini/CupList",
  }).catch((err) => {
    toast("获取杯子列表失败，请重试")
  });

  if (res?.Code !== 200) {
    return [];
  }
  return res?.Data;
}

// 图片上传
export async function uploadImg({ filePath, onSuccess, onFail, formData }: UploadProp) {
  Taro.uploadFile({
    url: "http://120.78.75.249:8081/logo/image/upload",
    name: "file",
    filePath: filePath,
    header: {
      "Content-Type": "multipart/form-data",
    },
    formData: formData,
    success: onSuccess,
    fail: onFail,
  });
}

// logo list
export async function getLogoList() {
  const res = await request<ResProp<LogoList>>({
    url: "logo/official/get",
    method: "GET",
  }, true);

  if (res?.Code !== 200) {
    return [];
  }
  return res?.Data;
}


