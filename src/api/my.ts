import { request } from "@/common/request";
import { setLoad } from "@/common/utils";
import Taro from "@tarojs/taro";

export async function getToken(code, shareToken?: string) {
  const res = await request<any>({
    url: "mini/WechatLogin",
    method: "POST",
    data: {
      code: code,
      guider: shareToken
    }
  });
  setLoad(
    'token',
    res?.Data?.token
  )
  if (res?.Code !== 200) {
    return [];
  }
  return res?.Data?.token;
}
