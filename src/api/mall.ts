import { CoffeeTypeList, ResProp } from "@/atypes";
import { request } from "@/common/request";

interface TagListRes {}
// 获取所有标签
export async function getTagList() {
  const data = await request<ResProp<TagListRes[]>>(
    {
      url: "tags/get",
    },
    true
  );
  if (data.Code !== 200) {
    return [];
  }
  return data;
}

// interface uploadImageRes {}
// // 上传图片--可能需要使用到Taro中得上传图片
// export async function uploadImage() {
//   const data = await request<ResProp<uploadImageRes>>(
//     {
//       url: "logo/image/upload ",
//       method: "POST",
//     },
//     true
//   );
//   if (data.Code !== 200) {
//     return [];
//   }
//   return data;
// }

interface addLogoRes {}
// 新增logo
export async function addLogo() {
  const data = await request<ResProp<addLogoRes>>(
    {
      url: "logo/add",
    },
    true
  );
  if (data.Code !== 200) {
    return [];
  }
  return data;
}
