import { request } from "@/common/request";
import { ResProp } from "@/atypes";
import { orderListRes } from "./my-order-list";

// 获取咖啡详情
export async function getCoffeeDetail(order_id: string) {
  const res = await request<ResProp<orderListRes>>({
    url: "mini/CoffeeOrderState",
    data: {
      order_id,
    },
  });

  if (res?.Code !== 200) {
    return;
  }
  return res?.Data;
}
