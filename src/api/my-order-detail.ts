import { ResProp } from "@/atypes";
import { request } from "@/common/request";

// 获取当前订单详情
export async function getCurrentOrderDetail() {
  const data = await request<ResProp<any[]>>({
    url: "mini/CoffeeMake ",
    data: {
      order_id: "", // 订单id
      is_query: true,
      device_id: "", // 设备id
      coffees: [  // 当前订单的咖啡信息
        {
          cup_model_id: "",
          coffee_model_id: "",
          logo_url: "",
          cool: "",
          sweet: "",
        },
      ],
    },
  });
  if (data.Code !== 200) {
    return [];
  }
  return data;
}
