import { showShareMenu, useShareAppMessage } from "@tarojs/taro";
import { useEffect } from "react";
import { shareIntercept } from "./utils";

const useShareIntercept = () => {

  useShareAppMessage(shareIntercept);
}

export default useShareIntercept