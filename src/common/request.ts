import { commonRequestRes } from "@/atypes";
import Taro from "@tarojs/taro";
import { getLoad } from "./utils";
import { toast } from "@/api/util";

interface IErrorCodeTip {
  Code: number;
  Msg: string;
}

export const request = <T>(options: Taro.request.Option, otherPort?: boolean) => {
  // const COMMONURL = "http://120.78.75.249:8001";
  const COMMONURL = "https://www.kocoffee.top";
  return new Promise<T>(async (resolve, rejects) => {
    await Taro.request<commonRequestRes<T>>({
      ...options,
      url: `${COMMONURL}/${options.url}`,
      method: options?.method || "GET",
      header: {
        "content-type": "application/json", // 默认值
        ...options?.header,
        Authorization: getLoad("token"),
        Mobile: getLoad('phoneNum') ?? '',
      },
      success: (res) => {
        // console.log(res, "请求打印");

        const { data } = res;
        if ((data as any)?.Code !== 200) {
          toast((data as any)?.Msg);
          rejects(res);
        }

        resolve(data as T);
      },
      fail: (err) => {
        console.log(err, "请求失败嘞");

        Taro.showToast({
          title: "请求失败",
          icon: "error",
          duration: 1000,
        });
        rejects(err);
      },
    });
  });
};
