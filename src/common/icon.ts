/* eslint-disable import/no-commonjs */
const icons = {
  // 选杯子图案2/3
  icon_hot: require("../assets/icon/icon_hot.png"),
  icon_hot_active: require("../assets/icon/icon_hot_active.png"),
  icon_collect: require("../assets/icon/icon_collect.png"),
  icon_collect_active: require("../assets/icon/icon_collect_active.png"),
  icon_upload: require("../assets/icon/icon_upload.png"),

  icon_upload_active: require("../assets/icon/icon_upload_active.png"),
  icon_more: require("../assets/icon/icon_more.png"),
  icon_buy_card: require("../assets/icon/icon_buy_card.png"),
  collect_empy: 'https://cj-cdn.oss-rg-china-mainland.aliyuncs.com/icon/collect_empy.png',
  // 下单
  icon_order_buy: 'https://cj-cdn.oss-rg-china-mainland.aliyuncs.com/icon/icon_order_buy.png',
  icon_order_custom: 'https://cj-cdn.oss-rg-china-mainland.aliyuncs.com/icon/icon_order_custom.png',

  icon_gwd: require("@/assets/icon/icon_gwd.png"),
  icon_btn_set: require("@/assets/icon/icon_btn_set.png"),
  img_pic4: require("@/assets/icon/img_pic4.png"),
  icon_order_logo: require("@/assets/icon/order_logo.png"),
  icon_btn_down: require("@/assets/icon/icon_btn_down.png"),
  icon_btn_down1: require("@/assets/icon/icon_btn_down_white.png"),
  // me
  icon_dd: require("@/assets/icon/icon_dd.png"),
  icon_dingzhi: require("@/assets/icon/icon_dingzhi.png"),
  icon_fabu: require("@/assets/icon/icon_fabu.png"),
  icon_jiameng: require("@/assets/icon/icon_jiameng.png"),
  icon_kefu: require("@/assets/icon/icon_kefu.png"),
  icon_shoucang: require("@/assets/icon/icon_shoucang.png"),
  icon_sp: require("@/assets/icon/icon_sp.png"),
  icon_sy: require("@/assets/icon/icon_sy.png"),
  icon_xieyi: require("@/assets/icon/icon_xieyi.png"),
  icon_yhq: require("@/assets/icon/icon_yhq.png"),
  icon_yinsi: require("@/assets/icon/icon_yinsi.png"),
  // 次卡详情
  bout_card: require("@/assets/icon/bout_card.jpg"),
  to_bout_card: require("@/assets/icon/to_bout_card.jpg"),

  icon_airplane: require("@/assets/icon/icon_airplane.png"),
  icon_upload2: require("@/assets/icon/icon_upload2.png"),
  icon_btn_down_white: require("@/assets/icon/icon_btn_down_white.png"),
  icon_btn_up_white: require("@/assets/icon/icon_btn_up_white.png"),
  icon_right: require("@/assets/icon/icon_right.png"),
  youhuika: require("@/assets/icon/youhuika.png"),
  youhuiquan: require("@/assets/icon/youhuiquan.png"),
  coupon_dialog: 'https://cj-cdn.oss-rg-china-mainland.aliyuncs.com/icon/coupon_dialog.png',
  coupon_bg: require("@/assets/icon/coupon_bg.png"),

  icon_fuli_default: require("@/assets/icon/icon_fuli_default.png"),
  icon_shequn: require("@/assets/icon/icon_shequn.png"),
  icon_yaoqing: require("@/assets/icon/icon_yaoqing.png"),
  icon_zhoubian: require("@/assets/icon/icon_zhoubian.png"),

  icon_game_entry: 'https://cj-cdn.oss-rg-china-mainland.aliyuncs.com/icon/icon_game_entry.png',
  icon_order_entry: 'https://cj-cdn.oss-rg-china-mainland.aliyuncs.com/icon/icon_order_entry.png',
  icon_right_arrow: require("@/assets/icon/icon_right_arrow.png"),
  icon_poster: require("@/assets/icon/icon_poster.jpg"),
  // invite
  icon_share: 'https://cj-cdn.oss-rg-china-mainland.aliyuncs.com/icon/share.png',
  icon_ko_new_bg: require("@/assets/icon/icon_ko_new_bg.jpg"),
  icon_invite_rewards: require("@/assets/icon/icon_invite_rewards.png"),
  icon_invite_complete: require("@/assets/icon/icon_invite_complete.png"),
  icon_invite_coupon: require("@/assets/icon/icon_invite_coupon.png"),
  icon_share_logo: require("@/assets/icon/icon_share_logo.png"),
  icon_arrow_right_fill: require("@/assets/icon/icon_arrow_right_fill.png"),
  icon_wx_logo: require("@/assets/icon/icon_wx_logo.png"),

  // 咖友社群
  img_kayou_bg: require("@/assets/icon/img_kayou_bg.png"),
  icon_location: require("@/assets/icon/icon_location.png"),

  img_chatroom: require('@/assets/icon/img_chatroom.png'),

};

export default icons;
