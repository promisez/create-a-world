import Taro, {
  ShareAppMessageReturn,
  getCurrentPages,
  useDidShow,
  useShareAppMessage,
} from "@tarojs/taro";
import icons from "./icon";

export const openPDF = (url: string, title: string) => {
  console.log(Taro.env.USER_DATA_PATH, "USER_DATA_PATH");
  Taro.showLoading({
    title: "正在打开...",
  });
  Taro.downloadFile({
    url,
    filePath: `${Taro.env.USER_DATA_PATH}/${title}.pdf`,
    success: function (res) {
      Taro.openDocument({
        filePath: res.filePath,
        fileType: "pdf",
        success(res) {
          Taro.hideLoading();
        },
      });
    },
  });
};

export const getLoad = (key: string) => {
  return Taro.getStorageSync(key);
};

export const setLoad = (key: string, value: any) => {
  return Taro.setStorageSync(key, value);
};

export const shareIntercept: () => ShareAppMessageReturn = () => {
  return {
    title: "邀您一起KO一杯～今日一切都OK！",
    imageUrl: icons.icon_share,
    path: `/pages/home/index?token=${getLoad("token")}`,
  };
};

export const displayShare = () => {
  Taro.showShareMenu({
    showShareItems: ["wechatFriends", "wechatMoment"],
  });
};

export const navTo = (url: string) => {
  const routers = getCurrentPages().map((v) => v.route);
  if (routers.length >= 10) {
    setLoad("routers", routers);
    Taro.reLaunch({ url });
    return;
  }
  Taro.navigateTo({ url });
};

export const navBack = () => {
  const routers = getCurrentPages().map((v) => v.route);
  const saveRouters = getLoad("routers");
  if (routers.length === 1) {
    if (saveRouters.length === 1) {
      Taro.navigateBack();
      return;
    }
    Taro.reLaunch({ url: saveRouters });
    saveRouters.pop();
    setLoad("routers", saveRouters);
    return;
  }
  Taro.navigateBack();
};

// 时间处理
export function showTimeFn(d: number | string, isAll = true) {
  const date = new Date(d);
  const YY = date.getFullYear();
  let MM: string | number = date.getMonth() + 1;
  let DD: string | number = date.getDate();
  let hh: string | number = date.getHours();
  let mm: string | number = date.getMinutes();
  let ss: string | number = date.getSeconds();

  if (hh < 10) {
    hh = `0${hh}`;
  }
  if (mm < 10) {
    mm = `0${mm}`;
  }
  if (ss < 10) {
    ss = `0${ss}`;
  }
  if (MM < 10) {
    MM = `0${MM}`;
  }
  if (DD < 10) {
    DD = `0${DD}`;
  }

  if (isAll) {
    return `${YY}-${MM}-${DD} ${hh}:${mm}:${ss}`;
  } else {
    return `${hh}:${mm}`;
  }
}

// 距离单位换算
export function distanceConverFn(distance: number){
  if(distance <= 1000){
    return `${distance.toFixed()}米`
  }else {
    return `${(distance / 1000).toFixed()}公里`
  }
}