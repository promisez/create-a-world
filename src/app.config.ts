export default defineAppConfig({
  pages: [
    "pages/home/index",
    // "pages/order/index",
    "pages/coffee/index",
    "pages/me/index",
    // "pages/mall/index",
  ],
  subPackages: [
    {
      name: "首页相关",
      root: "pages/HomeScreens/",
      pages: ["invite/index", "association/index", "chatRoomList/index", "chatroom/index"],
    },
    {
      name: "下单相关",
      root: "pages/OrderScreens/",
      pages: [
        // "coffee/index",
        "buycoffee/index",
        "favorable/index",
        "orderdetail/index",
        // "cup/index",
        // "buycup/index",
        // "ChooseImageList/index",
      ],
    },
    {
      name: "我的页面相关",
      root: "pages/MeScreens/",
      pages: [
        "orderlist/index",
        "cardRule/index",
        // "published/index",
        // "collected/index"
      ],
    },
    // {
    //   name: "商城相关",
    //   root: "pages/MallScreens/",
    //   pages: ["publish/index", "publishing/index"],
    // },
  ],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "KO咖啡",
    navigationBarTextStyle: "black",
    navigationStyle: "custom",
  },
  tabBar: {
    // custom: true, // 自定义导航
    color: "#A0A0A0", // 未选中的颜色
    selectedColor: "#333333", // 选中的颜色
    backgroundColor: "#FFFFFF", // 背景颜色
    list: [
      {
        pagePath: "pages/home/index",
        text: "首页",
        iconPath: "assets/tabbar/tab_icon_home.png",
        selectedIconPath: "assets/tabbar/tab_icon_home_active.png",
      },
      {
        pagePath: "pages/coffee/index",
        text: "下单",
        iconPath: "assets/tabbar/tab_icon_order.png",
        selectedIconPath: "assets/tabbar/tab_icon_order_active.png",
      },
      // {
      //   pagePath: "pages/mall/index",
      //   text: "商城",
      //   iconPath: "assets/tabbar/tab_icon_mall.png",
      //   selectedIconPath: "assets/tabbar/tab_icon_mall_active.png",
      // },
      {
        pagePath: "pages/me/index",
        text: "我的",
        iconPath: "assets/tabbar/tab_icon_my.png",
        selectedIconPath: "assets/tabbar/tab_icon_my_active.png",
      },
    ],
  },

  permission: {
    "scope.userLocation": {
      desc: "用于定位您附近的设备信息",
    },
  },
  requiredBackgroundModes: ["audio", "location"],
  requiredPrivateInfos: ["getLocation"],
  enableShareAppMessage: true,
  enableShareTimeline: true,
  enablePullDownRefresh: true,
  lazyCodeLoading: "requiredComponents",
});
