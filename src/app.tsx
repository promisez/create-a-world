// import "taro-ui/dist/style/index.scss";
// import "taro-ui/dist/style/components/icon.scss";
import Taro, {
  getCurrentPages,
  useDidShow,
  useRouter,
  useShareAppMessage,
  useShareTimeline,
} from "@tarojs/taro";

import React, { PropsWithChildren, useEffect, useMemo } from "react";
import { useLaunch } from "@tarojs/taro";
import "./app.less";
import { View } from "@tarojs/components";
import { displayShare } from "./common/utils";

function App({ children }: PropsWithChildren<any>) {
  useLaunch(() => {
    console.log("App launched.");
  });
  displayShare();
  // console.log(
  //   getCurrentPages().map((v) => v.route),
  //   "app.tsx 路由栈"
  // );

  // children 是将要会渲染的页面
  return children;
}

export default App;
