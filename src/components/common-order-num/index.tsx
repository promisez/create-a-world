import { View } from "@tarojs/components";
import React from "react";
import "./index.less";

interface CommonOrderNumProps {
  orderNumber: number;
  orderState: number | undefined;
}
export const stateMap = {
  0: "排队中",
  1: "制作中",
  2: "待取餐",
  3: "已完成订单",
};

function CommonOrderNum(props) {
  const { orderNumber, orderState, coffeeNum, TakeNum } = props;
  return (
    <View className="commonOrderNumContaner">
      <View className="orderNum">订单号: {orderNumber}</View>
      {typeof orderState !== "undefined" && (
        <View className={orderState === 3 ? "complete" : "incomplete"}>
          {`${stateMap[orderState]}${TakeNum ?? 0}/${coffeeNum ?? 1}`}
        </View>
      )}
    </View>
  );
}

export default CommonOrderNum;
