import { View, Image } from "@tarojs/components";
import React from "react";
import "./index.less";

function ShapeBadge(props) {
  const { title, text, index, actIndex, onClick, color, icon, actIcon } = props;
  const isActive = actIndex === index;
  console.log("actIndex", actIndex, "index", index);

  return (
    <View className={`shape-badge shape-badge-${isActive ? "active" : ""}`} onClick={onClick}>
      <View className='shape-image'><Image className='icon' src={isActive ? actIcon : icon} /></View>
      <View className="content">
        <View className="tabTitle" style={{ color: isActive ? color : "" }}>
          {title}
          <View className="priceText" style={{ color: isActive ? '#000' : '' }} >{text}</View>
        </View>
      </View>
    </View>
  );
}

export default ShapeBadge;
