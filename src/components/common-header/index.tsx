import { View, Text } from "@tarojs/components";
import Taro, { navigateBack } from "@tarojs/taro";
import { ArrowLeft } from "@taroify/icons";
import React from "react";
import "./index.less";
interface HProps {
  title: string;
  isShowBack?: boolean;
  onBack?: () => void;
  className?:string;
}
function Header({ title, isShowBack = true, onBack,className }: HProps) {
  const onHeaderBack = () => {
    onBack ? onBack() : navigateBack()
  }
  return (
    <View className={`commonHeader ${className}`}>
      {isShowBack && <ArrowLeft size={18} onClick={onHeaderBack} color="#000" />}
      <Text className="title">{title}</Text>
      {isShowBack && <View className="emptyplace" />}
    </View>
  );
}

export default Header;
