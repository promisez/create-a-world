import { ActionSheet, Backdrop, Button, Popup } from "@taroify/core";
import { Like, Star } from "@taroify/icons";
import { Image, View, Text } from "@tarojs/components";
import Taro from "@tarojs/taro";
import "./index.less";
import { useState } from "react";
import icons from "@/common/icon";
import PopUpHeader from "../popup-header";

interface ImageDetailProps {
  actIndex?: number;
  index: number;
  onClick?: () => void;
  showLike?: boolean;
  showCollect?: boolean;
  showUseCount?: boolean;
  showTag?: boolean;
  showPopUp?: boolean;
  data: {
    url: string;
    collect?: boolean;
    like?: boolean;
    title: string;
    tag?: string;
    useCount?: number;
  };
}

function ImageDetail(props: ImageDetailProps) {
  const {
    actIndex,
    index,
    onClick,
    data,
    showLike = true,
    showCollect = true,
    showUseCount = true,
    showTag = true,
    showPopUp = false,
  } = props;
  const { url, collect, like, title, tag = "夏日", useCount = "777" } = data;

  const [isCollect, setIsCollect] = useState(collect);
  const [isLike, setIsLike] = useState(like);
  const [open, setOpen] = useState(false);
  const [isPreviewOpen, setIsPreviewOpen] = useState(false);

  const onCollect = (e) => {
    e.stopPropagation();

    // await request then
    setIsCollect(!isCollect);
    Taro.showToast({
      title: !isCollect ? "收藏成功" : "收藏已取消",
      icon: "success",
      duration: 1000,
    });
  };
  const onLike = (e) => {
    e.stopPropagation();

    // await request then
    setIsLike(!isLike);
    Taro.showToast({
      title: !isLike ? "点赞成功" : "点赞已取消",
      icon: "success",
      duration: 1000,
    });
  };
  const onPreview = (e) => {
    e.stopPropagation();
    if (showPopUp) {
      setIsPreviewOpen(true);
      return;
    }
    Taro.previewImage({
      urls: [url],
    });
  };
  const onSheetSelect = (e) => {
    console.log(e.value, "onSheetSelect");
    if (e.value === "share") {
      // 调用分享接口
    }
    if (e.value === "delete") {
      // 调用删除接口
    }
    setOpen(false);
  };

  console.log(isCollect, "isCollect", isLike, "isLike");

  const containerName = `image-detail-container ${
    actIndex === index ? "image-detail-container-active" : ""
  }`;
  return (
    <View className={containerName} onClick={onClick}>
      <Image lazyLoad fadeIn src={url} preview="10" className="image-content" />
      <View className="detail-content">
        <View className="text-info-title">{title}</View>
        {showTag && <View className="detail-tag">{tag}</View>}
        <View className="text-info-content">
          <View className="text-info-action">
            {showCollect && (
              <View className="text-info-show">
                <View className="text-info-num">4564</View>
                <Star
                  className="text-info-img"
                  onClick={onCollect}
                  size="14"
                  color={isCollect ? "rgb(255,171,0)" : "rgb(220,220,220)"}
                />
              </View>
            )}
            {showLike && (
              <View className="text-info-show">
                <View className="text-info-num">789</View>
                <Like
                  className="text-info-img heart"
                  size="14"
                  onClick={onLike}
                  color={isLike ? "rgb(255,0,0)" : "rgb(220,220,220)"}
                />
              </View>
            )}
            <Button variant="text" className="text-info-preview" onClick={onPreview}>
              预览
            </Button>
            {!showCollect && !showLike && (
              <Image
                className="text-info-more"
                src={icons.icon_more}
                onClick={() => setOpen(true)}
              />
            )}
            <ActionSheet
              className="more-action-sheet"
              open={open}
              rounded={false}
              onSelect={onSheetSelect}
              onCancel={() => setOpen(false)}
              onClose={setOpen}
            >
              <ActionSheet.Action value="share" name="发布到共享广场" />
              <ActionSheet.Action value="delete" name="删除" />
              <ActionSheet.Button type="cancel">取消</ActionSheet.Button>
            </ActionSheet>
          </View>
          {showUseCount && <View className="text-info-use">被使用{useCount}次</View>}
        </View>
      </View>
      <Backdrop
        className="previewBackdrop"
        open={isPreviewOpen}
        closeable
        onClose={() => setIsPreviewOpen(false)}
      >
        <Popup
          className="previewPopup"
          open={isPreviewOpen}
          placement="bottom"
          style={{ height: "90%" }}
        >
          <PopUpHeader onClick={() => setIsPreviewOpen(false)} />
          <View className="content">
            <Text className="title">白色兔子</Text>
            <View className="text-info-show">
              <View className="text-info-num">789</View>
              <Like
                className="text-info-img heart"
                size="14"
                onClick={onLike}
                color={isLike ? "rgb(255,0,0)" : "rgb(220,220,220)"}
              />
            </View>
          </View>
          <View className="descrition">文字介绍文字介绍文字介绍</View>
          <View>list</View>
        </Popup>
      </Backdrop>
    </View>
  );
}

export default ImageDetail;
