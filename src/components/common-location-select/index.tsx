import icons from "@/common/icon";
import { setLoad } from "@/common/utils";
import { View, Text, Image } from "@tarojs/components";
import React, { useEffect, useState } from "react";
import "./index.less";

function CommonLocationSelect(props) {
  const { localtionArr = [], changeLocationFn = (it) => {}, currentId } = props;
  const [regionIndex, setRegionIndex] = useState("");

  useEffect(() => {
    setRegionIndex(currentId);
  }, [currentId]);

  const selectRegion = (e, item) => {
    e.stopPropagation();
    changeLocationFn(item);
    setLoad("region", item);
    setRegionIndex(item?.device_id);
  };
  return (
    <View className="commonLocationContainer">
      {localtionArr?.map((item, index) => {
        return (
          <View
            className={`item ${regionIndex === item?.device_id ? "item-active" : ""}`}
            key={index}
            onClick={(e) => selectRegion(e, item)}
          >
            <View className="itemLeft">
              <View className="regionIcon">
                <Image src={icons.icon_location} className="locationBg" />
              </View>
              <View className="regionContent">
                <View className="regionInfo">
                  <Text className="regionName">{item.address}</Text>
                  <Text className="regionTip"></Text>
                </View>
                <Text className="regionAddress">{item.name}</Text>
              </View>
            </View>
            {regionIndex === item?.device_id && (
              <View className="itemRight">
                <Text className="currentSelect">当前选择</Text>
              </View>
            )}
          </View>
        );
      })}
    </View>
  );
}

export default CommonLocationSelect;
