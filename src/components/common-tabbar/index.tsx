import { View, Image } from "@tarojs/components";
import React, { useEffect, useState } from "react";
import homeIcon from "../../assets/tabbar/tab_icon_home.png";
import homeIconSelected from "../../assets/tabbar/tab_icon_home_active.png";
import orderIcon from "../../assets/tabbar/tab_icon_order.png";
import orderIconSelected from "../../assets/tabbar/tab_icon_order_active.png";
import meIcon from "../../assets/tabbar/tab_icon_my.png";
import meIconSelected from "../../assets/tabbar/tab_icon_my_active.png";
import "./index.less";

const tabbarList = [
  {
    pagePath: "pages/home/index",
    text: "首页",
    iconPath: homeIcon,
    selectedIconPath: homeIconSelected,
    tag: "home",
  },
  {
    pagePath: "pages/order/index",
    text: "下单",
    iconPath: orderIcon,
    selectedIconPath: orderIconSelected,
    tag: "order",
  },
  {
    pagePath: "pages/me/index",
    text: "我的",
    iconPath: meIcon,
    selectedIconPath: meIconSelected,
    tag: "me",
  },
];

interface ICommonTabbarProps {
  children: React.ReactNode;
  cerrentPage: string;
}
function CommonTabbar(props: ICommonTabbarProps) {
  const { children, cerrentPage } = props;

  return (
    <View className="commonTabbar">
      <View className="container">{children}</View>
      <View className="bottomTabbar">
        {tabbarList?.map((item, index) => {
          console.log(
            item.tag === cerrentPage ? "active default" : "default",
            item.tag,
            cerrentPage
          );

          return (
            <View key={index} className={item.tag === cerrentPage ? "active default" : "default"}>
              <Image
                className="icons"
                src={item.tag === cerrentPage ? item.selectedIconPath : item.iconPath}
              />
              <View className="text">{item.text}</View>
            </View>
          );
        })}
      </View>
    </View>
  );
}

export default CommonTabbar;
