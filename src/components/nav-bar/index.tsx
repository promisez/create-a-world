import { shareIntercept } from "@/common/utils";
import { View } from "@tarojs/components";
import Taro, { useShareAppMessage } from "@tarojs/taro";
import React, { useEffect, useMemo } from "react";

function NavTopBar() {
  const safeAreaTop = useMemo(() => {
    const info = Taro.getWindowInfo();
    return info.safeArea?.top;
  }, []);
  useShareAppMessage(shareIntercept);

  return <View style={{ background: "#fff", paddingTop: safeAreaTop ? safeAreaTop : 44 }} />;
}

export default React.memo(NavTopBar);
