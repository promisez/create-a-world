import { View, Image } from "@tarojs/components";
import React from "react";
import "./index.less";
import icons from "@/common/icon";

function PopUpHeader({ onClick }) {
  const onClose = (e) => {
    e.stopPropagation();
    onClick && onClick();
  };
  return (
    <View className="popUpHeader" onClick={onClose}>
      <Image className="popHeaderImg" src={icons.icon_btn_down}></Image>
    </View>
  );
}

export default PopUpHeader;
