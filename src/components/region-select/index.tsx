import React, { useMemo } from "react";
import { Location } from "@taroify/icons";
import { PageContainer, Text, View } from "@tarojs/components";
import Taro, { useDidShow, useShareAppMessage } from "@tarojs/taro";
import { useEffect, useState } from "react";

import { LocationList } from "@/atypes";
import PopUpHeader from "@/components/popup-header";
import { distanceConverFn, getLoad, setLoad, shareIntercept } from "@/common/utils";
import "./index.less";
import OrderRegion from "@/pages/OrderScreens/components/order-step-title";

function RegionSelect(props) {
  const { onClose, regionList, selectRegion, regionInfo } = props;

  const regionIndex = useMemo(() => {
    const index = regionList?.findIndex((v, i) => v.device_id === regionInfo.device_id);
    return index;
  }, [regionInfo]);

  useShareAppMessage(shareIntercept);

  return (
    <View className="backdropBox">
      <View className="regionListBox">
        {regionList.map((item, index) => {
          return (
            <View
              className={`item ${regionIndex === index ? "item-active" : ""}`}
              key={index}
              onClick={(e) => {
                selectRegion(e, index);
                setTimeout(() => {
                  onClose();
                }, 300);
              }}
            >
              <View className="itemLeft">
                <View className="regionIcon">
                  <Location color="red" size={14} />
                </View>
                <View className="regionContent">
                  <View className="regionInfo">
                    <Text className="regionName">{item.address}</Text>
                    <Text className="regionTip"></Text>
                  </View>
                  <Text className="regionAddress">{item.name}</Text>
                </View>
              </View>
              {regionIndex === index && (
                <View className="itemRight">
                  <Text className="currentSelect">当前选择</Text>
                </View>
              )}
            </View>
          );
        })}
      </View>
    </View>
  );
}

export default RegionSelect;
